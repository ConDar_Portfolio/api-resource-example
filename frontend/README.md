# Getting Started
To start the project, first run the `SampleWebApi` project packaged alongside this app.
The API will start on https port 7139 by default, if this is changed for any reason you should update the `.env.local` file to update the API url.

Now run `npm install` to download and install all dependencies for the app and then `npm start` to start it running.

# Project Structure

## `.env.local`
This app uses the `.env` pattern to define environment variables for the application at runtime, in particular `.env.local` is a local override file and shouldn't be checked into source control.
Only variables defined with the prefix `REACT_APP_` will be added to the `process.env` object at runtime.
I do not know if multiple `.env` files are merged like `appsettings` in C#, but I assume they are.

See here for additional details: https://create-react-app.dev/docs/adding-custom-environment-variables/

## `ApiResourceExamples`
This component is the framework for the multiple examples available, sets up the tabs, the title and description of each example, and then renders the example component

## `common`
This directory contains the common components, types and other code that is used in each version.

### `BasicAuthAddon`
This is a custom addon for `wretch` that adds the ability to add basic authorization to a request.

## `v*`
These directories (`v1`, `v2`, `v-final`, etc...) contain the code for each version of the api resource examples. Each version was copied from the previous version before updates were made.

### `Example.tsx`
Each version contains an `Example.tsx` file which defines it's entrypoint `Example` component and it's `VersionExample` configuration - it is this configuration that is pulled out by the `ApiResourceExamples` component to render the example in it's framework.