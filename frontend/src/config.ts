export const config = {
    api: {
        url: process.env.REACT_APP_API_URL as string,
        auth: {
            username: process.env.REACT_APP_API_AUTH_USERNAME as string,
            password: process.env.REACT_APP_API_AUTH_PASSWORD as string,
        },
    },
};