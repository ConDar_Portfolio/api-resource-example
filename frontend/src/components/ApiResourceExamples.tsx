import classNames from "classnames";
import { useState } from "react";
import { AccordionBody, AccordionHeader, AccordionItem, Col, Nav, NavItem, NavLink, Row, TabContent, TabPane, UncontrolledAccordion } from "reactstrap";
import { VersionExample } from "./common/types";
import { versionFinal } from "./v-final/Example";
import { versionOne } from "./v1/Example";
import { versionTwo } from "./v2/Example";
import { versionThree } from "./v3/Example";
import { versionFour } from "./v4/Example";
import { versionFive } from "./v5/Example";
import { versionSix } from "./v6/Example";
import { versionSeven } from "./v7/Example";
import { versionEight } from "./v8/Example";

export const ApiResourceExamples = () => {
    const [selectedVersion, setSelectedVersion] = useState(versionExamples[0].version);

    return <>
        <Row>
            <Col className='text-center'>
                <h1 className='display-1'>Evolution of the ApiResource</h1>
            </Col>
        </Row>
        <Row>
            <Col>
                <h3>Versions:</h3>
            </Col>
        </Row>
        <Row>
            <Col>
                <Nav tabs>
                    {versionExamples.map(example => (
                        <NavItem key={example.version}>
                            <NavLink
                                className={classNames({'active': selectedVersion === example.version})}
                                onClick={() => setSelectedVersion(example.version)}
                            >
                                {example.version}
                            </NavLink>
                        </NavItem>
                    ))}
                </Nav>
            </Col>
        </Row>
        <Row>
            <Col>
                <TabContent activeTab={selectedVersion}>
                    {versionExamples.map(example => (
                        <TabPane key={example.version} tabId={example.version}>
                            {selectedVersion === example.version && <VersionExampleView {...example}/>}
                        </TabPane>
                    ))}
                </TabContent>
            </Col>
        </Row>
    </>
}

const VersionExampleView = ({title, description, component: VersionComponent}: VersionExample) => {

    // Accordian is incorrectly typed for typescript, so we use an empty props cast to any to get round it.
    const accordianProps = { } as any;

    return <>
        <Row>
            <Col>
                <h1 className='display-3'>{title}</h1>
            </Col>
        </Row>
        <Row>
            <Col>
                <UncontrolledAccordion {...accordianProps}>
                    <AccordionItem>
                        <AccordionHeader targetId='description'>
                            Description
                        </AccordionHeader>
                        <AccordionBody accordionId='description'>
                            {description}
                        </AccordionBody>
                    </AccordionItem>
                </UncontrolledAccordion>
            </Col>
        </Row>
        <Row className='my-2'>
            <Col>
                <hr/>
            </Col>
        </Row>
        <Row>
            <Col>
                <VersionComponent/>
            </Col>
        </Row>
    </>
}

const versionExamples: VersionExample[] = [
    versionOne,
    versionTwo,
    versionThree,
    versionFour,
    versionFive,
    versionSix,
    versionSeven,
    versionEight,
    versionFinal,
]