import React, { ReactNode } from "react";
import { WretchError } from "wretch";
import { LoadingSpinner } from "../common/LoadingSpinner";
import { useApiResource } from "./useApiResource";

export type ApiResourceRequestProps = {
    resource: string,
    queryParameters?: Record<string, any>,
    headers?: Record<string, any>
}

type ErrorRendererCallbacks = {
    renderDefault: (error: WretchError) => ReactNode,
    reload: () => void,
}

export type ErrorRenderer = (error: WretchError, callbacks: ErrorRendererCallbacks) => ReactNode

export type ApiResourceRenderProps = {
    renderLoading?: () => ReactNode,
    renderError?: ErrorRenderer,
}

type ApiResourceProps<T> = ApiResourceRequestProps & ApiResourceRenderProps & {
    children: (data: T, reload: () => void) => React.ReactNode,
}

export const ApiResource = <T extends unknown>(props: ApiResourceProps<T>) => {
    const {
        resource,
        queryParameters,
        headers,

        renderLoading = defaultRenderLoading,
        renderError = defaultRenderError,

        children,
    } = props;

    const [state, reloadResource] = useApiResource<T>(resource, {queryParameters, headers});

    switch (state.status) {
        case 'pending':
            return <>{renderLoading()}</>

        case 'error':
            console.log(state.error);
            return <>{renderError(state.error, {renderDefault: defaultRenderError, reload: reloadResource})}</>

        case 'success':
            return <>{children(state.data, reloadResource)}</>
    }
}

const defaultRenderLoading = () => <LoadingSpinner/>
const defaultRenderError = (error: WretchError) => (
    <h1 className='display-3 text-danger'>
        Error with status code {error.status}
    </h1>
)