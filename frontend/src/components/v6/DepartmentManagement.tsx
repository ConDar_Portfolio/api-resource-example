import classNames from "classnames";
import { Col, Row } from "reactstrap";
import { DepartmentSelect } from "../common/DepartmentSelect";
import { Department, Organization } from "../common/types";
import { ApiResource } from "./ApiResource";

type DepartmentManagementProps = {
    organization: Organization,
    setDepartment: (department: Department | undefined) => void,
    className?: string,
}

export const DepartmentManagement = ({organization, setDepartment, className}: DepartmentManagementProps) => {
    return (
        <Row className={classNames(className, 'justify-content-end')}>
            <Col xs={4}>
                <ApiResource resource='/departments' queryParameters={{organizationId: organization.id}}>
                    {(departments: Department[]) => (
                        <DepartmentSelect departments={departments} onSelected={setDepartment}/>
                    )}
                </ApiResource>
            </Col>
        </Row>
    )
}