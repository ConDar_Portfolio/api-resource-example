import { useCallback, useState } from "react";
import { Col, FormGroup, Input, Label, Row } from "reactstrap";
import { Department, Organization, VersionExample } from "../common/types";
import { DepartmentManagement } from "./DepartmentManagement";
import { EmployeeManagement } from "./EmployeeManagement";
import { OrganizationManagement } from "./OrganizationManagement";

const Example = () => {
    const [selectedOrganization, setSelectedOrganization] = useState<Organization | undefined>(undefined);
    const [selectedDepartment, setSelectedDepartment] = useState<Department | undefined>(undefined);

    const [enableErrorRenderer, setEnableErrorRenderer] = useState(false);

    const setOrganization = useCallback((organization: Organization | undefined) => {
        setSelectedOrganization(organization);
        setSelectedDepartment(undefined);
    }, [setSelectedOrganization, setSelectedDepartment]);

    const toggleEnableErrorRenderer = useCallback(
        () => setEnableErrorRenderer(current => !current),
        [setEnableErrorRenderer]);

    return <>
        <Row>
            <Col>
                <FormGroup switch>
                    <Input id='enable-error-renderer-switch' type='checkbox' role='switch' checked={enableErrorRenderer} onChange={toggleEnableErrorRenderer}/>
                    <Label for='enable-error-renderer-switch' check>
                        Custom error renderer {enableErrorRenderer ? 'enabled' : 'disabled'}
                    </Label>
                </FormGroup>
            </Col>
        </Row>
        <OrganizationManagement setOrganization={setOrganization} className='mt-4'/>
        {selectedOrganization && <>
            <DepartmentManagement
                className='mt-4'
                organization={selectedOrganization}
                setDepartment={setSelectedDepartment}/>
            <EmployeeManagement
                className='mt-4'
                organization={selectedOrganization}
                department={selectedDepartment}
                enableErrorRenderer={enableErrorRenderer}/>
        </>}
    </>
}

export const versionSix: VersionExample = {
    version: 6,
    title: 'Custom render on error',
    description: <>
        <p>
            This version we consider the case where we upgrade to using version <code>2.0</code> of the employees API, this is done by passing the
            <code>api-resource</code> header with value <code>2.0</code>, for which we need to update both <code>useApiResource</code>
            and <code>ApiResource</code>.
        </p>
        <p>
            Version <code>2.0</code> of the employees API has a different behaviour than version <code>1.0</code> when there are no employees
            found - while version <code>1.0</code> returned an empty list, version <code>2.0</code> returns a <code>NotFound</code> error response.
            However we still want the same behaviour as before when there are no employees to list, so we introduce the <code>renderError</code>
            render prop into the <code>ApiResource</code> which will allow for custom error rendering (in this case the empty employees list).
        </p>
        <p>
            The <code>renderError</code> prop has two arguments, the error that caused it to be called, and an object of callbacks containing
            a reference to the default render so that once special cases have been accounted for any unhandled errors can be handled in the
            default manner, and the reload function that can be used to trigger the reload of the resource.
        </p>
        <p>
            <em>Note:</em> This version uses a simple switch to turn on and off the custom error renderer, but it will be simply on by
            default in subsequent versions.
        </p>
    </>,
    component: Example,
}