import { Col, Row } from "reactstrap"
import { EmployeeList } from "../common/EmployeeList"
import { Department, Employee, Organization } from "../common/types"
import { ApiResource, ErrorRenderer } from "./ApiResource"

type EmployeeManagementProps = {
    organization: Organization,
    department: Department | undefined,
    className?: string,
    enableErrorRenderer: boolean,
}

export const EmployeeManagement = ({organization, department, className, enableErrorRenderer}: EmployeeManagementProps) => {
    const queryParameters = department === undefined
        ? { organizationId: organization.id }
        : { departmentId: department.id }

    const renderError = enableErrorRenderer ? renderEmployeesError : undefined

    return (
        <Row className={className}>
            <Col>
                <ApiResource resource='/employees' queryParameters={queryParameters} headers={{'api-version': '2.0'}} renderError={renderError}>
                    {(employees: Employee[]) => (
                        <EmployeeList employees={employees}/>
                    )}
                </ApiResource>
            </Col>
        </Row>
    )
}

const renderEmployeesError: ErrorRenderer = (error, {renderDefault}) => (
    error.status === 404
        ? <EmployeeList employees={[]}/>
        : renderDefault(error)
)