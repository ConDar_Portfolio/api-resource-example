import classNames from "classnames";
import { Col, Row } from "reactstrap";
import { DepartmentSelect } from "../common/DepartmentSelect";
import { LoadingSpinner } from "../common/LoadingSpinner";
import { Department, Organization } from "../common/types";
import { useApiResource } from "./useApiResource";

type DepartmentManagementProps = {
    organization: Organization,
    setDepartment: (department: Department | undefined) => void,
    className?: string,
}

export const DepartmentManagement = ({organization, setDepartment, className}: DepartmentManagementProps) => {
    const state = useApiResource<Department[]>('/departments', {organizationid: organization.id});

    if (state.status === 'pending')
        return <LoadingSpinner/>

    if (state.status === 'error') {
        console.error(state.error);
        return (
            <h1 className='display-3 text-danger'>
                Error with status code {state.error.status}
            </h1>
        )
    }

    return (
        <Row className={classNames(className, 'justify-content-end')}>
            <Col xs={4}>
                <DepartmentSelect departments={state.data} onSelected={setDepartment}/>
            </Col>
        </Row>
    )
}