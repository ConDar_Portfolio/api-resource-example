import { Col, Row } from "reactstrap"
import { EmployeeList } from "../common/EmployeeList"
import { LoadingSpinner } from "../common/LoadingSpinner"
import { Department, Employee, Organization } from "../common/types"
import { useApiResource } from "./useApiResource"

type EmployeeManagementProps = {
    organization: Organization,
    department: Department | undefined,
    className?: string,
}

export const EmployeeManagement = ({organization, department, className}: EmployeeManagementProps) => {
    const state = useApiResource<Employee[]>(
        '/employees',
        department === undefined
            ? { organizationId: organization.id }
            : { departmentId: department.id });

    if (state.status === 'pending')
        return <LoadingSpinner/>

    if (state.status === 'error') {
        console.error(state.error);
        return (
            <h1 className='display-e text-danger'>
                Error with status code {state.error.status}
            </h1>
        )
    }

    return (
        <Row className={className}>
            <Col>
                <EmployeeList employees={state.data}/>
            </Col>
        </Row>
    )
}