import { useCallback, useState } from "react";
import { Department, Organization, VersionExample } from "../common/types";
import { DepartmentManagement } from "./DepartmentManagement";
import { EmployeeManagement } from "./EmployeeManagement";
import { OrganizationManagement } from "./OrganizationManagement";

const Example = () => {
    const [selectedOrganization, setSelectedOrganization] = useState<Organization | undefined>(undefined);
    const [selectedDepartment, setSelectedDepartment] = useState<Department | undefined>(undefined);

    const setOrganization = useCallback((organization: Organization | undefined) => {
        setSelectedOrganization(organization);
        setSelectedDepartment(undefined);
    }, [setSelectedOrganization, setSelectedDepartment]);

    return <>
        <OrganizationManagement setOrganization={setOrganization}/>
        {selectedOrganization && <>
            <DepartmentManagement
                className='mt-4'
                organization={selectedOrganization}
                setDepartment={setSelectedDepartment}/>
            <EmployeeManagement
                className='mt-4'
                organization={selectedOrganization}
                department={selectedDepartment}/>
        </>}
    </>
}

export const versionTwo: VersionExample = {
    version: 2,
    title: 'Introducing a useApiResource hook',
    description: <>
        <p>
            By introducing the <code>useApiResource</code> hook this version removes a lot of duplicated code for the boilerplate
            of fetching a resource using <code>wretch</code>. However there is still a lot of code duplication around how to render
            the loading and error states.
        </p>
        <p>
            There is a very dirty use of <code>JSON.stringify</code> in the <code>useMemo</code> deps list in <code>useApiResource</code>.
            This does allow us to prevent unnecessary rerenders without relying on the calling code to memoize the query parameters, which is
            why it is implemented, even if bad general practice
        </p>
        <p>
            The <code>isCancelled</code> value within the <code>useEffect</code> in <code>useApiResource</code> is very important to avoid
            setting stale data as the api call is asynchronously. If the effect within <code>useApiResource</code> would be re-run before
            an outstanding request is completed then the <code>isCancelled</code> is set to true (because the function the effect returns will be run)
            and then no further data or error will be set as it's instead being handled by a new request.
        </p>
    </>,
    component: Example,
}