import { useCallback, useState } from "react";
import { Col, Row } from "reactstrap";
import { LoadingSpinner } from "../common/LoadingSpinner";
import { OrganizationCard } from "../common/OrganizationCard";
import { OrganizationSelect } from "../common/OrganizationSelect";
import { Organization } from "../common/types";
import { useApiResource } from "./useApiResource";

type OrganizationManagementProps = {
    setOrganization: (organization: Organization | undefined) => void,
    className?: string,
}

export const OrganizationManagement = ({setOrganization, className}: OrganizationManagementProps) => {
    const state = useApiResource<Organization[]>('/organizations');
    const [selectedOrganization, setSelectedOrganization] = useState<Organization | undefined>(undefined);

    const onSelected = useCallback((organization: Organization) => {
        setSelectedOrganization(organization);
        setOrganization(organization);
    }, [setSelectedOrganization, setOrganization]);

    if (state.status === 'pending')
        return <LoadingSpinner/>

    if (state.status === 'error') {
        console.error(state.error);
        return (
            <h1 className='display-3 text-danger'>
                Error with status code {state.error.status}
            </h1>
        )
    }

    return (
        <Row className={className}>
            <Col>
                <OrganizationSelect organizations={state.data} onSelected={onSelected}/>
            </Col>
            {selectedOrganization && (
                <Col>
                    <OrganizationCard organization={selectedOrganization}/>
                </Col>
            )}
        </Row>
    )
}