import { useCallback, useEffect, useMemo, useState } from "react";
import { WretchError } from "wretch";
import QueryStringAddon from "wretch/addons/queryString";
import { useApi } from "./useApi";

type ApiResourceState<T> =
    | { status: 'pending' }
    | { status: 'success', data: T }
    | { status: 'error', error: WretchError }

type UseApiResourceValue<T> = [ApiResourceState<T>, () => void]

type ApiResourceOptions = {
    queryParameters?: Record<string, any>,
    headers?: Record<string, any>,
}

export const useApiResource = <T>(url: string, options: ApiResourceOptions = {}): UseApiResourceValue<T> => {
    const {
        queryParameters,
        headers,
    } = options;

    const [state, setState] = useState<ApiResourceState<T>>({status: 'pending'});
    const [reloadTrigger, setReloadTrigger] = useState<{}>({});
    const api = useApi();

    const memoizedQueryParameters = useMemo(
        () => queryParameters,
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [JSON.stringify(queryParameters)]);

    const memoizedHeaders = useMemo(
        () => headers,
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [JSON.stringify(headers)]
    )

    useEffect(() => {
        setState({status: 'pending'});

        let isCancelled = false;
        let localApi = api.url(url);

        const makeApiCall = async () => {
            if (memoizedQueryParameters) {
                // any cast required to stop typescript complaining
                localApi = localApi
                    .addon(QueryStringAddon)
                    .query(memoizedQueryParameters) as any;
            }

            if (memoizedHeaders) {
                localApi = localApi.headers(memoizedHeaders);
            }

            localApi
                .get()
                .json((data: T) => {
                    setTimeout(() => {
                        !isCancelled && setState({status: 'success', data});
                    }, 250);

                    return data;
                })
                .catch((error: WretchError) => {
                    if (isCancelled) {  
                        return;
                    }

                    setState({status: 'error', error})
                });
        }

        makeApiCall();

        return () => {
            isCancelled = true;
        }
    }, [setState, api, url, memoizedQueryParameters, memoizedHeaders, reloadTrigger])

    const reloadResource = useCallback(() => setReloadTrigger({}), [setReloadTrigger]);

    return [state, reloadResource];
}