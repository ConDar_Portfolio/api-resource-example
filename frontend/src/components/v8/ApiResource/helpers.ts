import { cloneElement } from "react";
import { ApiResourceChildren } from "./types";

export const renderApiResourceChildren = <T extends unknown>(children: ApiResourceChildren<T>, data: T, reloadData: () => void) => {
    return children instanceof Function
        ? children(data, reloadData)
        : cloneElement(children, {data, reloadData});
}