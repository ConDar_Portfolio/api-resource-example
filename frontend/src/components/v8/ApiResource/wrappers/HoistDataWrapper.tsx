import { ReactNode, useEffect } from 'react';
import { ApiResourceChildProps } from "../types";

type HoistDataWrapperProps<T> = ApiResourceChildProps<T> & {
    setData: (data: T) => void,
    children: ReactNode | ((reloadData: () => void) => ReactNode),
}

export const HoistDataWrapper = <T extends unknown>({data, reloadData, setData, children}: HoistDataWrapperProps<T>) => {
    useEffect(() => setData(data as T), [setData, data]);

    return <>{children instanceof Function ? children(reloadData as () => void) : children}</>
}