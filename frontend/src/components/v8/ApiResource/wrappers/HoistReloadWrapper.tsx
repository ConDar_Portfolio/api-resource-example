import { useEffect } from "react";
import { renderApiResourceChildren } from "../helpers";
import { ApiResourceChildWrapperProps } from "../types";

type HoistReloadWrapperProps<T> = ApiResourceChildWrapperProps<T> & {
    setReload: (reload: () => void) => void,
}

export const HoistReloadWrapper = <T extends unknown>({setReload, data, reloadData, children}: HoistReloadWrapperProps<T>) => {
    useEffect(() => setReload(reloadData as () => void), [setReload, reloadData]);

    return <>{renderApiResourceChildren(children, data as T, reloadData as () => void)}</>
}