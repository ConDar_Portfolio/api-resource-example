import { useCallback, useState } from "react"
import { Col, Row } from "reactstrap"
import { EmployeeList } from "../common/EmployeeList"
import { Department, Employee, Organization } from "../common/types"
import { ApiResource } from "./ApiResource"
import { ErrorRenderer } from "./ApiResource/types"
import { HoistDataWrapper } from "./ApiResource/wrappers"
import { CreateEmployeeButton } from "./CreateEmployeeButton"

type EmployeeManagementProps = {
    organization: Organization,
    department: Department | undefined,
    className?: string,
}

export const EmployeeManagement = ({organization, department, className}: EmployeeManagementProps) => {
    const queryParameters = department === undefined
        ? { organizationId: organization.id }
        : { departmentId: department.id }

    const [employees, setEmployees] = useState<Employee[]>([]);

    const addEmployee = useCallback((newEmployee: Employee) => {
        setEmployees(current => [...current, newEmployee]);
    }, [setEmployees]);

    return <>
        <Row className={className}>
            <Col>
                <ApiResource resource='/employees' queryParameters={queryParameters} headers={{'api-version': '2.0'}} renderError={renderEmployeesError}>
                    <HoistDataWrapper setData={setEmployees}>
                        <EmployeeList employees={employees}/>
                    </HoistDataWrapper>
                </ApiResource>
            </Col>
        </Row>
        <Row>
            <Col>
                <CreateEmployeeButton className='mt-3' color='primary' organization={organization} addEmployee={addEmployee} department={department}>
                    Create Employee
                </CreateEmployeeButton>
            </Col>
        </Row>
    </>
}

const renderEmployeesError: ErrorRenderer = (error, {renderDefault}) => (
    error.status === 404
        ? <EmployeeList employees={[]}/>
        : renderDefault(error)
)