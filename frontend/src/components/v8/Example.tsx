import { useCallback, useState } from "react";
import { Department, Organization, VersionExample } from "../common/types";
import { DepartmentManagement } from "./DepartmentManagement";
import { EmployeeManagement } from "./EmployeeManagement";
import { OrganizationManagement } from "./OrganizationManagement";

const Example = () => {
    const [selectedOrganization, setSelectedOrganization] = useState<Organization | undefined>(undefined);
    const [selectedDepartment, setSelectedDepartment] = useState<Department | undefined>(undefined);

    const setOrganization = useCallback((organization: Organization | undefined) => {
        setSelectedOrganization(organization);
        setSelectedDepartment(undefined);
    }, [setSelectedOrganization, setSelectedDepartment]);

    return <>
        <OrganizationManagement setOrganization={setOrganization}/>
        {selectedOrganization && <>
            <DepartmentManagement
                className='mt-4'
                organization={selectedOrganization}
                setDepartment={setSelectedDepartment}/>
            <EmployeeManagement
                className='mt-4'
                organization={selectedOrganization}
                department={selectedDepartment}/>
        </>}
    </>
}

export const versionEight: VersionExample = {
    version: 8,
    title: 'Hoist the fetched data',
    description: <>
        <p>
            This version is an iteration on the previous version. When a new employee is created the endpoint returns the newly created <code>Employee</code>
            {} object back as the response body, given we have an array of <code>Employee</code>s to render the employee list we want a way to update that
            list on a new employee being created instead of reloading the list.
        </p>
        <p>
            To acheive this we build on the concept of the API Resource wrappers by introducing the <code>HoistDataWrapper</code>. This wrapper will, similar
            to the <code>HoistReloadWrapper</code> be rendered when the <code>ApiResource</code> is rendered and set the fetched data externally
            using the <code>setData</code> prop. The <code>children</code> prop of the <code>HoistDataWrapper</code> is either a function taking the
            reload data function or a <code>ReactNode</code>, in particular if the code under the <code>ApiResource</code> needs the fetched data it
            should use the data set using the <code>setData</code> prop.
        </p>
    </>,
    component: Example,
}