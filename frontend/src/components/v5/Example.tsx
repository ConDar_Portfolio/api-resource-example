import { useCallback, useState } from "react";
import { Department, Organization, VersionExample } from "../common/types";
import { DepartmentManagement } from "./DepartmentManagement";
import { EmployeeManagement } from "./EmployeeManagement";
import { OrganizationManagement } from "./OrganizationManagement";

const Example = () => {
    const [selectedOrganization, setSelectedOrganization] = useState<Organization | undefined>(undefined);
    const [selectedDepartment, setSelectedDepartment] = useState<Department | undefined>(undefined);

    const setOrganization = useCallback((organization: Organization | undefined) => {
        setSelectedOrganization(organization);
        setSelectedDepartment(undefined);
    }, [setSelectedOrganization, setSelectedDepartment]);

    return <>
        <OrganizationManagement setOrganization={setOrganization}/>
        {selectedOrganization && <>
            <DepartmentManagement
                className='mt-4'
                organization={selectedOrganization}
                setDepartment={setSelectedDepartment}/>
            <EmployeeManagement
                className='mt-4'
                organization={selectedOrganization}
                department={selectedDepartment}/>
        </>}
    </>
}

export const versionFive: VersionExample = {
    version: 5,
    title: 'Reload functionality',
    description: <>
        <p>
            This version we consider a design requirement that we need to trigger a reload of the fetched organizations - suppose we know that new organizations
            can be added externally and we want to update the current data in response. To support this we update the <code>useApiResource</code> hook to
            return both the current state and a function to reload the data, this reload function is then passed to the <code>children</code> function
            when rendering an <code>ApiResource</code> and can subsequently be called to trigger the reload of the data.
        </p>
        <p>
            The <code>useApiResource</code> hook now has a <code>reloadTrigger</code> state that is just an empty object, and is uesd as a
            dep in the deps list of the <code>useEffect</code>. Whenever the <code>reloadResource</code> function is called it sets
            the <code>reloadTrigger</code> value to a new instance of an empty object, this then causes the <code>useEffect</code> code to re-run.
        </p>
    </>,
    component: Example,
}