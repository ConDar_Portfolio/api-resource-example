import { Col, Row } from "reactstrap"
import { EmployeeList } from "../common/EmployeeList"
import { Department, Employee, Organization } from "../common/types"
import { ApiResource } from "./ApiResource"

type EmployeeManagementProps = {
    organization: Organization,
    department: Department | undefined,
    className?: string,
}

export const EmployeeManagement = ({organization, department, className}: EmployeeManagementProps) => {
    const queryParameters = department === undefined
        ? { organizationId: organization.id }
        : { departmentId: department.id }

    return (
        <Row className={className}>
            <Col>
                <ApiResource resource='/employees' queryParameters={queryParameters}>
                    {(employees: Employee[]) => (
                        <EmployeeList employees={employees}/>
                    )}
                </ApiResource>
            </Col>
        </Row>
    )
}