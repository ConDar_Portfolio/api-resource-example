import { Col, ListGroup, ListGroupItem, Row } from "reactstrap"
import { Employee } from "./types"

type EmployeeListProps = {
    employees: Employee[],
}

export const EmployeeList = ({employees}: EmployeeListProps) => {
    if (employees.length === 0)
        return <h1 className='display-3'>No Employees</h1>

    return (
        <ListGroup>
            {employees.map(employee => (
                <ListGroupItem key={employee.id}>
                    <Row>
                        <Col>
                            <strong>Name:</strong> {employee.firstName} {employee.lastName}
                        </Col>
                        <Col>
                            <strong>Salary:</strong> {Intl.NumberFormat('en-gb', {style: 'currency', currency: 'GBP'}).format(employee.salary)}
                        </Col>
                    </Row>
                </ListGroupItem>
            ))}
        </ListGroup>
    )
}