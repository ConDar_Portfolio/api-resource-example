import { Buffer } from 'buffer';
import { Wretch, WretchAddon } from "wretch";

export interface IBasicAuthAddon {
    basicAuth<T extends IBasicAuthAddon, C, R>(this: T & Wretch<T, C, R>, username: string, password: string): this;
}

export const BasicAuthAddon : WretchAddon<IBasicAuthAddon> = {
    wretch: {
        basicAuth(username, password) {
            const tokenString = `${username}:${password}`;
            const token = Buffer.from(tokenString).toString('base64');

            return this.auth(`Basic ${token}`);
        }
    }
}