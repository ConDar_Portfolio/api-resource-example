import { ComponentType, ReactNode } from "react"

export type Organization = {
    id: number,
    name: string,
    type: string,
    revenue: number,
}

export type Department = {
    id: number,
    organizationId: number,
    name: string,
}

export type Employee = {
    id: number,
    departmentId: number,
    firstName: string,
    lastName: string,
    salary: number,
}

export type VersionExample = {
    version: number | string,
    title: string,
    description: ReactNode,
    component: ComponentType,
}