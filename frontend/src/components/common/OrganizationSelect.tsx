import { ChangeEventHandler, useCallback } from "react"
import { Input } from "reactstrap"
import { Organization } from "./types"

const noOrganizationOption = 'no-organization-selected-option'

type OrganizationSelectProps = {
    organizations: Organization[],
    onSelected: (organization: Organization) => void,
}

export const OrganizationSelect = ({organizations, onSelected}: OrganizationSelectProps) => {

    const onChange = useCallback<ChangeEventHandler<HTMLInputElement>>(event => {
        const selectedValue = event.target.value;
        if (selectedValue === noOrganizationOption) {
            return;
        }

        const organizationId = parseInt(selectedValue);
        const organization = organizations.find(organization => organization.id === organizationId);

        if (organization === undefined) {
            return;
        }

        onSelected(organization)
    }, [organizations, onSelected]);

    return (
        <Input id='organization-select' name='organization-select' type='select' onChange={onChange} defaultValue={noOrganizationOption}>
            <option value={noOrganizationOption} disabled>Select organization...</option>
            {organizations.map(organization => <option key={organization.id} value={organization.id}>{organization.name}</option>)}
        </Input>
    )
}