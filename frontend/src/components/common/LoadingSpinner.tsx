import { Spinner } from "reactstrap";

export const LoadingSpinner = () => (
    <Spinner color='primary' type='border' style={{width: '3rem', height: '3rem'}}/>
)