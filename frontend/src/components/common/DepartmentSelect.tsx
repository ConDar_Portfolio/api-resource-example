import { ChangeEventHandler, useCallback } from "react";
import { Input } from "reactstrap";
import { Department } from "./types";

const noDepartmentOption = 'no-department-option';

type DepartmentSelectProps = {
    departments: Department[],
    onSelected: (department: Department | undefined) => void,
}

export const DepartmentSelect = ({departments, onSelected}: DepartmentSelectProps) => {

    const onChange = useCallback<ChangeEventHandler<HTMLInputElement>>(event => {
        const selectedValue = event.target.value;
        if (selectedValue === noDepartmentOption) {
            onSelected(undefined);
            return;
        }

        const departmentId = parseInt(selectedValue);
        const department = departments.find(department => department.id === departmentId);

        if (department === undefined) {
            onSelected(undefined);
            return;
        }

        onSelected(department);
    }, [departments, onSelected]);

    return (
        <Input id='department-select' name='department-select' type='select' onChange={onChange}>
            <option value={noDepartmentOption}>No department selected</option>
            {departments.map(department => <option key={department.id} value={department.id}>{department.name}</option>)}
        </Input>
    )
}