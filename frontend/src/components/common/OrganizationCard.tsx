import { Card, CardBody, CardHeader, Col, Row } from "reactstrap"
import { Organization } from "./types"

type OrganizationCardProps = {
    organization: Organization,
}

export const OrganizationCard = ({organization}: OrganizationCardProps) => (
    <Card>
        <CardHeader>
            {organization.name}
        </CardHeader>
        <CardBody>
            <Row>
                <Col>
                    <strong>Type:</strong> {organization.type}
                </Col>
                <Col>
                    <strong>Revenue:</strong> {Intl.NumberFormat('en-gb', {style: 'currency', currency: 'GBP'}).format(organization.revenue)}
                </Col>
            </Row>
        </CardBody>
    </Card>
)