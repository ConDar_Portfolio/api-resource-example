import { useCallback, useState } from "react";
import { Department, Organization, VersionExample } from "../common/types";
import { DepartmentManagement } from "./DepartmentManagement";
import { EmployeeManagement } from "./EmployeeManagement";
import { OrganizationManagement } from "./OrganizationManagement";

const Example = () => {
    const [selectedOrganization, setSelectedOrganization] = useState<Organization | undefined>(undefined);
    const [selectedDepartment, setSelectedDepartment] = useState<Department | undefined>(undefined);

    const setOrganization = useCallback((organization: Organization | undefined) => {
        setSelectedOrganization(organization);
        setSelectedDepartment(undefined);
    }, [setSelectedOrganization, setSelectedDepartment]);

    return <>
        <OrganizationManagement setOrganization={setOrganization}/>
        {selectedOrganization && <>
            <DepartmentManagement
                className='mt-4'
                organization={selectedOrganization}
                setDepartment={setSelectedDepartment}/>
            <EmployeeManagement
                className='mt-4'
                organization={selectedOrganization}
                department={selectedDepartment}/>
        </>}
    </>
}

export const versionThree: VersionExample = {
    version: 3,
    title: 'Introducing the ApiResource component',
    description: <>
        <p>
            By introducing the <code>ApiResource</code> component that uses the <code>useApiResource</code> hook we have almost entirely
            reduced code duplication by letting the <code>ApiResource</code> handle the loading and error states. The next versions will
            focus on new requirements coming in that require extensions of the <code>useApiResource</code> and <code>ApiResource</code> functionality.
        </p>
        <p>
            The <code>children</code> prop of a component is a special prop that will be set to the content rendered as the children of the
            component. While in react the standard type for <code>children</code> is <code>ReactNode</code> (string, number, <code>JSX.Element</code>, etc...)
            it can be any type we want, so we define that the type of <code>children</code> should be a function taking the fetched data and returning a
            {} <code>ReactNode</code>. This allows us to render our components that depend on the fetched data in a sensible way as they will always be passed
            the latest fetched data that they can use to render the desired components.
        </p>
    </>,
    component: Example,
}