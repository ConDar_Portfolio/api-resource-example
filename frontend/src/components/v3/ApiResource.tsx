import { LoadingSpinner } from "../common/LoadingSpinner";
import { useApiResource } from "./useApiResource";

type ApiResourceRequestProps = {
    resource: string,
    queryParameters?: Record<string, any>,
}

type ApiResourceProps<T> = ApiResourceRequestProps & {
    children: (data: T) => React.ReactNode,
}

export const ApiResource = <T extends unknown>(props: ApiResourceProps<T>) => {
    const {
        resource,
        queryParameters,
        children,
    } = props;

    const state = useApiResource<T>(resource, queryParameters);

    switch (state.status) {
        case 'pending':
            return <LoadingSpinner/>

        case 'error':
            console.log(state.error);
            return (
                <h1 className='display-3 text-danger'>
                    Error with status code {state.error.status}
                </h1>
            )

        case 'success':
            return <>{children(state.data)}</>
    }
}