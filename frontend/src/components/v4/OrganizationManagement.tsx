import { useCallback, useState } from "react";
import { Col, Placeholder, Row } from "reactstrap";
import { OrganizationCard } from "../common/OrganizationCard";
import { OrganizationSelect } from "../common/OrganizationSelect";
import { Organization } from "../common/types";
import { ApiResource } from "./ApiResource";

type OrganizationManagementProps = {
    setOrganization: (organization: Organization | undefined) => void,
    className?: string,
}

export const OrganizationManagement = ({setOrganization, className}: OrganizationManagementProps) => {
    const [selectedOrganization, setSelectedOrganization] = useState<Organization | undefined>(undefined);

    const onSelected = useCallback((organization: Organization) => {
        setSelectedOrganization(organization);
        setOrganization(organization);
    }, [setSelectedOrganization, setOrganization]);

    return (
        <Row className={className}>
            <Col>
                <ApiResource resource='/organizations' renderLoading={renderOrganizationsLoading}>
                    {(organizations: Organization[]) => (
                        <OrganizationSelect organizations={organizations} onSelected={onSelected}/>
                    )}
                </ApiResource>
            </Col>
            {selectedOrganization && (
                <Col>
                    <OrganizationCard organization={selectedOrganization}/>
                </Col>
            )}
        </Row>
    )
}

const renderOrganizationsLoading = () => (
    <Placeholder color='secondary' xs={12} size='lg' className='placeholder-wave'/>
)