import { useCallback, useState } from "react";
import { Department, Organization, VersionExample } from "../common/types";
import { DepartmentManagement } from "./DepartmentManagement";
import { EmployeeManagement } from "./EmployeeManagement";
import { OrganizationManagement } from "./OrganizationManagement";

const Example = () => {
    const [selectedOrganization, setSelectedOrganization] = useState<Organization | undefined>(undefined);
    const [selectedDepartment, setSelectedDepartment] = useState<Department | undefined>(undefined);

    const setOrganization = useCallback((organization: Organization | undefined) => {
        setSelectedOrganization(organization);
        setSelectedDepartment(undefined);
    }, [setSelectedOrganization, setSelectedDepartment]);

    return <>
        <OrganizationManagement setOrganization={setOrganization}/>
        {selectedOrganization && <>
            <DepartmentManagement
                className='mt-4'
                organization={selectedOrganization}
                setDepartment={setSelectedDepartment}/>
            <EmployeeManagement
                className='mt-4'
                organization={selectedOrganization}
                department={selectedDepartment}/>
        </>}
    </>
}

export const versionFour: VersionExample = {
    version: 4,
    title: 'Custom render while loading',
    description: <>
        <p>
            This version we consider a design requirement that when the list of organizations are loading it should show a placeholder for the select
            instead of the standard loading spinner. We acheive this by adding what is known as a render prop, which is a way of being able
            to provide a custom render for a given situation, in this case while loading.
        </p>
        <p>
            This is the most basic example of a render prop, but later versions will introduce new render props to be seen later.
        </p>
        <p>
            <em>Note:</em> in this iteration we artificially introduce a 1000ms delay when fetching data to better see how the render prop works, this
            delay will be reduced but not removed in future versions.
        </p>
    </>,
    component: Example,
}