import { createContext, Dispatch, PropsWithChildren, useContext, useReducer } from "react";
import { exampleDataReducer } from "./reducer";
import { ExampleData, ExampleDataAction } from "./types";

const initialExampleData: ExampleData = {
    organizations: [],
    selectedOrganization: undefined,
    departments: [],
    selectedDepartment: undefined,
    employees: [],
}

const exampleDataContext = createContext<[ExampleData, Dispatch<ExampleDataAction>]>([initialExampleData, () => {}]);

export const ExampleDataProvider = ({children}: PropsWithChildren) => {
    const exampleDataContextValue = useReducer(exampleDataReducer, {...initialExampleData});

    return (
        <exampleDataContext.Provider value={exampleDataContextValue}>
            {children}
        </exampleDataContext.Provider>
    )
}

export const useExampleData = () => useContext(exampleDataContext);