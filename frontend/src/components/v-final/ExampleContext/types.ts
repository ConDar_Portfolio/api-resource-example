import { Department, Employee, Organization } from "../../common/types";

export type ExampleData = {
    organizations: Organization[],
    selectedOrganization: Organization | undefined,
    departments: Department[],
    selectedDepartment: Department | undefined,
    employees: Employee[],
}

export type ExampleDataAction =
    | { type: 'set-organizations', organizations: Organization[] }
    | { type: 'set-selected-organization', organization: Organization | undefined }
    | { type: 'set-departments', departments: Department[] }
    | { type: 'set-selected-department', department: Department | undefined }
    | { type: 'set-employees', employees: Employee[] }
    | { type: 'add-employee', employee: Employee }