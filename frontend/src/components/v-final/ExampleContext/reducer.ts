import { ExampleData, ExampleDataAction } from "./types";

export const exampleDataReducer = (data: ExampleData, action: ExampleDataAction): ExampleData => {
    switch (action.type) {
        case 'set-organizations':
            return {
                ...data,
                organizations: action.organizations,
                selectedOrganization: undefined,
                departments: [],
                selectedDepartment: undefined,
                employees: [],
            };

        case 'set-selected-organization':
            return {
                ...data,
                selectedOrganization: action.organization,
                departments: [],
                selectedDepartment: undefined,
                employees: [],
            };

        case 'set-departments':
            return {
                ...data,
                departments: action.departments,
                selectedDepartment: undefined,
                employees: [],
            };

        case 'set-selected-department':
            return {
                ...data,
                selectedDepartment: action.department,
                employees: [],
            }

        case 'set-employees':
            return {
                ...data,
                employees: action.employees,
            }

        case 'add-employee':
            return {
                ...data,
                employees: data.selectedDepartment === undefined || data.selectedDepartment.id === action.employee.departmentId
                    ? [...data.employees, action.employee]
                    : data.employees,
            }
    }
}