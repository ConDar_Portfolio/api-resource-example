import classNames from "classnames";
import { useCallback } from "react";
import { Col, Row } from "reactstrap";
import { DepartmentSelect } from "../common/DepartmentSelect";
import { Department } from "../common/types";
import { ApiResource } from "./ApiResource";
import { HoistDataWrapper } from "./ApiResource/wrappers";
import { useExampleData } from "./ExampleContext";

type DepartmentManagementProps = {
    className?: string,
}

export const DepartmentManagement = ({className}: DepartmentManagementProps) => {
    const [{
        selectedOrganization,
        departments,
    }, dispatch] = useExampleData();

    const setDepartments = useCallback(
        (departments: Department[]) => dispatch({type: 'set-departments', departments}),
        [dispatch],
    );

    const setDepartment = useCallback(
        (department: Department | undefined) => dispatch({type: 'set-selected-department', department}),
        [dispatch]
    );

    return (
        <Row className={classNames(className, 'justify-content-end')}>
            <Col xs={4}>
                <ApiResource resource='/departments' queryParameters={{organizationId: selectedOrganization!.id}}>
                    <HoistDataWrapper setData={setDepartments}>
                        <DepartmentSelect departments={departments} onSelected={setDepartment}/>
                    </HoistDataWrapper>
                </ApiResource>
            </Col>
        </Row>
    )
}