import { VersionExample } from "../common/types";
import { DepartmentManagement } from "./DepartmentManagement";
import { EmployeeManagement } from "./EmployeeManagement";
import { ExampleDataProvider, useExampleData } from "./ExampleContext";
import { OrganizationManagement } from "./OrganizationManagement";

const ExampleWrapper = () => {
    return (
    <ExampleDataProvider>
        <Example/>
    </ExampleDataProvider>
)}

const Example = () => {
    const [{selectedOrganization}] = useExampleData();

    return <>
        <OrganizationManagement/>
        {selectedOrganization && <>
            <DepartmentManagement className='mt-4'/>
            <EmployeeManagement className='mt-4'/>
        </>}
    </>
}

export const versionFinal: VersionExample = {
    version: 'final',
    title: 'Final version refactor',
    description: <>
        <p>
            This final iteration does not add any new functionality that is visible to the user, but instead is a clean up refactor of the code
            and change to how the data is managed. This gives a better structure to the code making it more in line with the SOLID princicples
            and reduces prop drilling through the use of a context for the example data.
        </p>
        <p>
            The change to the data management is to introduce a <code>React.Context</code> backed by the <code>useReducer</code> hook
            which now manages all data centrally, the organization list, the selected organization, etc... The <code>useReducer</code>
            {} hook accepts a function called a <code>reducer</code> which is a pure function (a function that always returns the same output
            for the same input and has no side effects) that will map the existing state and an action to a new instance of the state.
            The use of a reducer is helpful for managing complicated state, such as here where selecting a organization should clear out the
            previous array of dpeartments and employees, etc...
            In our case we use a discriminated union for the type of the action that can be taken on the state, which means that code that calls
            the <code>dispatch</code> method will only compile if it provides one of the valid options for an action.
        </p>
        <p>
            Another refactor that was done was to introduce a new hook, the <code>useMemoDeepCompare</code> hook. This hook memoizes a value
            by using a deep compare (e.g. if it's an array comparing all array values with the same deep compare, or if it's an object comparing
            all of it's keys using the deep compare), which is better practice and more consistent than using <code>JSON.stringify</code>.
            This has the same effect of the prvious code (when used in <code>useApiResource</code>) to ensure that the client code does
            not need to memoize the query parameters or headers before sending them to the <code>useApiResource</code> hook.
        </p>
        <p>
            Continuing the refactor, the <code>useApiResource</code> hook has been refactored to use custom hooks within the file that break
            down it's logic into more manageable pieces. The <code>useConfiguredApi</code> hook creates a memoized <code>Wretch</code> object
            with the url, query parameters and headers already set. The <code>useMakeApiCall</code> hook takes in the <code>api</code> created
            using the <code>useConfiguredApi</code> hook and returns a memoized function that when called performs the actual api request.
            Combining the two this means that the <code>useEffect</code> callback has significantly fewer and simpler dependencies and
            code due to extracting a lot of the logic into the above mentioned custom hooks.
        </p>
        <p>
            The <code>CreateEmployeeButton</code> was the final code that got refactored, this again was primarily aimed at extracting
            logic to make each part of the code have a single responsibility and a little bit of code deduplication. In particular one
            change was to extract the concept of a <code>FormGroupComponent</code> which is a component with predefined components
            that allows each individual form group to be extracted to a separate file and combined into the modal by using a <code>.map</code>.
            The benefit of this is that if another form group is needed it can be added to it's own file, added to the <code>formGroups</code> array
            and the form will just simply work with the new group. Additionally the action that creates the employee has been extracted
            to it's own <code>actions</code> file to create a separation of concerns between the code to render the button/form and the
            logic of calling the API to create the employee.
        </p>
    </>,
    component: ExampleWrapper,
}