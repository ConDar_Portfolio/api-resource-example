import { Dispatch, SetStateAction, useCallback, useEffect, useMemo, useState } from "react";
import { Wretch, WretchError } from "wretch";
import QueryStringAddon from "wretch/addons/queryString";
import { useApi } from "./useApi";
import { useMemoDeepCompare } from "./useMemoDeepCompare";

type ApiResourceState<T> =
    | { status: 'pending' }
    | { status: 'success', data: T }
    | { status: 'error', error: WretchError }

type UseApiResourceValue<T> = [ApiResourceState<T>, () => void]

type ApiResourceOptions = {
    queryParameters?: Record<string, any>,
    headers?: Record<string, any>,
}

export const useApiResource = <T>(url: string, options: ApiResourceOptions = {}): UseApiResourceValue<T> => {
    const [state, setState] = useState<ApiResourceState<T>>({status: 'pending'});
    const [reloadTrigger, setReloadTrigger] = useState<{}>({});

    const api = useConfiguredApi(url, options);
    const makeApiCall = useMakeApiCall(api, setState);

    useEffect(() => {
        setState({status: 'pending'});

        let isCancelled = false;

        makeApiCall(() => isCancelled);

        return () => {
            isCancelled = true;
        }
    }, [setState, makeApiCall, reloadTrigger]);

    const reloadResource = useCallback(() => setReloadTrigger({}), [setReloadTrigger]);

    return [state, reloadResource];
}

const useConfiguredApi = (url: string, {queryParameters, headers}: ApiResourceOptions) => {
    const baseApi = useApi();

    const memoizedQueryParameters = useMemoDeepCompare(queryParameters);
    const memoizedHeaders = useMemoDeepCompare(headers);

    return useMemo(() => {
        let api = baseApi.url(url);

        if (memoizedQueryParameters) {
            api = api.addon(QueryStringAddon)
                .query(memoizedQueryParameters) as Wretch;
        }

        if (memoizedHeaders) {
            api = api.headers(memoizedHeaders);
        }

        return api;
    }, [baseApi, url, memoizedQueryParameters, memoizedHeaders]);
}

const useMakeApiCall = <T extends unknown>(api: Wretch, setState: Dispatch<SetStateAction<ApiResourceState<T>>>) => {
    return useCallback((isCancelled: () => boolean) => {
        api.get()
            .json((data: T) => {
                setTimeout(() => {
                    !isCancelled() && setState({status: 'success', data});
                }, 250);

                return data;
            })
            .catch((error: WretchError) => {
                !isCancelled() && setState({status: 'error', error});
            });
    }, [api, setState])
}