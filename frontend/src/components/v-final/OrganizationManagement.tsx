import { useCallback } from "react";
import { Button, Col, Placeholder, Row } from "reactstrap";
import { OrganizationCard } from "../common/OrganizationCard";
import { OrganizationSelect } from "../common/OrganizationSelect";
import { Organization } from "../common/types";
import { ApiResource } from "./ApiResource";
import { HoistDataWrapper } from "./ApiResource/wrappers";
import { useExampleData } from "./ExampleContext";

type OrganizationManagementProps = {
    className?: string,
}

export const OrganizationManagement = ({className}: OrganizationManagementProps) => {
    const [{organizations, selectedOrganization}, dispatch] = useExampleData();

    const setOrganizations = useCallback(
        (organizations: Organization[]) => dispatch({type: 'set-organizations', organizations}),
        [dispatch],
    );

    const setSelectedOrganization = useCallback(
        (organization: Organization) => dispatch({type: 'set-selected-organization', organization}),
        [dispatch],
    );

    return (
        <Row className={className}>
            <Col>
                <ApiResource resource='/organizations' renderLoading={renderOrganizationsLoading}>
                    <HoistDataWrapper setData={setOrganizations}>
                        {reload => {
                            const onClick = () => {
                                reload();
                                dispatch({type: 'set-organizations', organizations: []});
                            }

                            return <>
                                <OrganizationSelect organizations={organizations} onSelected={setSelectedOrganization}/>
                                <Button color='primary' className='mt-2' onClick={onClick}>Reload</Button>
                            </>
                        }}
                    </HoistDataWrapper>
                </ApiResource>
            </Col>
            {selectedOrganization && (
                <Col>
                    <OrganizationCard organization={selectedOrganization}/>
                </Col>
            )}
        </Row>
    )
}

const renderOrganizationsLoading = () => (
    <Placeholder color='secondary' xs={12} size='lg' className='placeholder-wave'/>
)