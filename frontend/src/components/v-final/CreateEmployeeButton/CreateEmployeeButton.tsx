import { useState } from "react";
import { DeepPartial, useForm } from "react-hook-form";
import { Button, ButtonProps, Form, Modal, ModalBody, ModalHeader } from "reactstrap";
import { useExampleData } from "../ExampleContext";
import { useApi } from "../useApi";
import { createEmployee } from "./actions";
import { noDepartmentSelectedId } from "./constants";
import { formGroups } from "./form-groups";
import { NewEmployeeFormData } from "./types";

export const CreateEmployeeButton = (props: ButtonProps) => {
    const [{selectedDepartment}, dispatch] = useExampleData();

    const [isOpen, setIsOpen] = useState(false);
    const api = useApi();

    const initialFormData: DeepPartial<NewEmployeeFormData> = {
        departmentId: selectedDepartment === undefined ? noDepartmentSelectedId : selectedDepartment.id,
    }

    const formContext = useForm<NewEmployeeFormData>({
        defaultValues: initialFormData,
    });

    const onSubmit = formContext.handleSubmit(formData => {
        createEmployee(
            api,
            formData,
            {
                onSuccess: employee => dispatch({type: 'add-employee', employee}),
                onCompleted: () => {
                    formContext.reset(initialFormData);
                    setIsOpen(false);
                },
            },
        )
    });

    const toggleOpen = () => {
        formContext.reset(initialFormData);
        setIsOpen(current => !current);
    }

    return <>
        <Button {...props} onClick={toggleOpen}/>
        <Modal isOpen={isOpen} toggle={toggleOpen}>
            <ModalHeader>Create new employee</ModalHeader>
            <ModalBody>
                <Form onSubmit={onSubmit}>
                    {formGroups.map((Group, index) => (
                        <Group key={index} {...formContext}/>
                    ))}
                </Form>
            </ModalBody>
        </Modal>
    </>
}