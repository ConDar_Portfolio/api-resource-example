import { FormFeedback, FormGroup, Input, Label } from "reactstrap";
import { useExampleData } from "../../ExampleContext";
import { noDepartmentSelectedId } from "../constants";
import { FormGroupComponent } from "../types";

export const DepartmentFormGroup: FormGroupComponent = ({register, formState: {errors}}) => {
    const [{departments}] = useExampleData();

    const {ref, ...rest} = register('departmentId', {
        required: true,
        valueAsNumber: true,
        min: 1,
    });

    return (
        <FormGroup>
            <Label for='departmentId'>Department:</Label>
            <Input id='departmentId' type='select' invalid={errors.departmentId !== undefined} innerRef={ref} {...rest}>
                <option value={noDepartmentSelectedId} disabled>Select department....</option>
                {departments.map(department => (
                    <option key={department.id} value={department.id}>{department.name}</option>
                ))}
            </Input>
            <FormFeedback>
                A department must be selected
            </FormFeedback>
        </FormGroup>
    )
}