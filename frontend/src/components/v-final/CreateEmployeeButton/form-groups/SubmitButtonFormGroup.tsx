import { Button, FormGroup } from "reactstrap";
import { FormGroupComponent } from "../types";

export const SubmitButtonFormGroup: FormGroupComponent = () => (
    <FormGroup>
        <Button color='primary' type='submit'>Create</Button>
    </FormGroup>
)