import { ComponentProps } from "react";
import { FormFeedback, FormGroup, Input, Label } from "reactstrap";
import { FormGroupComponent } from "../types";

type NameProperty = 'firstName' | 'lastName';

const NameFormGroup = ({nameProperty, register, formState: {errors}}: ComponentProps<FormGroupComponent> & {nameProperty: NameProperty}) => {
    const {ref, ...rest} = register(nameProperty, {
        required: true,
        maxLength: 50,
    });

    const labelText = namePropertyLabelTexts[nameProperty];

    return (
        <FormGroup>
            <Label for='firstName'>{labelText}:</Label>
            <Input id='firstName' type='text' invalid={errors[nameProperty] !== undefined} innerRef={ref} {...rest}/>
            <FormFeedback>
                {labelText} is required and must be at most 50 characters
            </FormFeedback>
        </FormGroup>
    )
}

const namePropertyLabelTexts: Record<NameProperty, string> = {
    firstName: 'First name',
    lastName: 'Last name',
}

export const FirstNameFormGroup: FormGroupComponent = formContext => (
    <NameFormGroup nameProperty='firstName' {...formContext}/>
);

export const LastNameFormGroup: FormGroupComponent = formContext => (
    <NameFormGroup nameProperty='lastName' {...formContext}/>
);