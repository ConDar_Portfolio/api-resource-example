import { FormGroupComponent } from '../types';
import { DepartmentFormGroup } from './DepartmentFormGroup';
import { FirstNameFormGroup, LastNameFormGroup } from './NameFormGroup';
import { SalaryFormGroup } from './SalaryFormGroup';
import { SubmitButtonFormGroup } from './SubmitButtonFormGroup';

export const formGroups: FormGroupComponent[] = [
    FirstNameFormGroup,
    LastNameFormGroup,
    DepartmentFormGroup,
    SalaryFormGroup,
    SubmitButtonFormGroup,
]