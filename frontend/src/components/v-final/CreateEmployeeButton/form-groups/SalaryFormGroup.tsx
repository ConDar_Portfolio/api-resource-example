import classNames from "classnames";
import { FormFeedback, FormGroup, Input, InputGroup, InputGroupText, Label } from "reactstrap";
import { FormGroupComponent } from "../types";

export const SalaryFormGroup: FormGroupComponent = ({register, formState: {errors}}) => {
    const {ref, ...rest} = register('salary', {
        required: true,
        valueAsNumber: true,
        min: 1,
        validate: salary => !isNaN(salary),
    });

    return (
        <FormGroup>
            <Label for='salary'>Salary:</Label>
            <InputGroup>
                <InputGroupText>£</InputGroupText>
                <Input id='salary' type='text' invalid={errors.salary !== undefined} innerRef={ref} {...rest}/>
            </InputGroup>
            <FormFeedback className={classNames({'d-block': errors.salary !== undefined})}>
                A salary of at least £1 is required
            </FormFeedback>
        </FormGroup>
    )
}