import { WretchError } from "wretch/resolver";
import { Wretch } from "wretch/types";
import { Employee } from "../../common/types";
import { NewEmployeeFormData } from "./types";

export const createEmployee = (
    api: Wretch,
    formData: NewEmployeeFormData,
    callbacks: {
        onSuccess: (newEmployee: Employee) => void,
        onError?: (error: WretchError) => void,
        onCompleted?: () => void,
    },
) => {
    const {
        onSuccess,
        onError = console.error,
        onCompleted = () => {},
    } = callbacks;

    api.url('/employees')
        .post(formData)
        .json((newEmployee: Employee) => {
            onSuccess(newEmployee);
            return newEmployee;
        })
        .catch(onError)
        .finally(onCompleted);
}