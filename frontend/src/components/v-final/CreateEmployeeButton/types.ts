import { ComponentType } from "react";
import { UseFormReturn } from "react-hook-form";

export type NewEmployeeFormData = {
    firstName: string,
    lastName: string,
    departmentId: number,
    salary: number,
};

export type FormGroupComponent = ComponentType<UseFormReturn<NewEmployeeFormData>>;