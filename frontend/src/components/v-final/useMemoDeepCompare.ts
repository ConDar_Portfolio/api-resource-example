import deepEqual from "deep-equal";
import { useRef } from "react";

export const useMemoDeepCompare = <T extends unknown>(value: T | undefined): T | undefined => {
    const memoizedValue = useRef<T | undefined>(undefined);

    if (!deepEqual(value, memoizedValue.current)) {
        memoizedValue.current = value;
    }

    return memoizedValue.current;
}