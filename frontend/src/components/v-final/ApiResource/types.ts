import { ReactElement, ReactNode } from "react"
import { WretchError } from "wretch"

export type ApiResourceRequestProps = {
    resource: string,
    queryParameters?: Record<string, any>,
    headers?: Record<string, any>
}

type ErrorRendererCallbacks = {
    renderDefault: (error: WretchError) => ReactNode,
    reload: () => void,
}

export type ErrorRenderer = (error: WretchError, callbacks: ErrorRendererCallbacks) => ReactNode

export type ApiResourceRenderProps = {
    renderLoading?: () => ReactNode,
    renderError?: ErrorRenderer,
}

export type ApiResourceChildren<T> = ((data: T, reload: () => void) => ReactNode) | ReactElement<ApiResourceChildProps<T>>;

export type ApiResourceChildProps<T = unknown> = {
    data?: T,
    reloadData?: () => void,
}

export type ApiResourceChildWrapperProps<T> = ApiResourceChildProps<T> & {
    children: ApiResourceChildren<T>;
}