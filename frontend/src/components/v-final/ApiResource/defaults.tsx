import { WretchError } from "wretch/resolver"
import { LoadingSpinner } from "../../common/LoadingSpinner"

export const defaultRenderLoading = () => <LoadingSpinner/>

export const defaultRenderError = (error: WretchError) => (
    <h1 className='display-3 text-danger'>
        Error with status code {error.status}
    </h1>
)