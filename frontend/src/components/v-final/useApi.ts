import { useMemo } from "react"
import wretch, { Wretch } from "wretch"
import { config } from "../../config"
import { BasicAuthAddon } from "../common/BasicAuthAddon"

const createApi = (): Wretch => {
    return wretch(config.api.url)
        .addon(BasicAuthAddon)
        .basicAuth(config.api.auth.username, config.api.auth.password) as Wretch;
}

export const useApi = (): Wretch => useMemo(createApi, []);