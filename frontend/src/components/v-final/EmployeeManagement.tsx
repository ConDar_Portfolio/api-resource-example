import { useCallback } from "react"
import { Col, Row } from "reactstrap"
import { EmployeeList } from "../common/EmployeeList"
import { Employee } from "../common/types"
import { ApiResource } from "./ApiResource"
import { ErrorRenderer } from "./ApiResource/types"
import { HoistDataWrapper } from "./ApiResource/wrappers/HoistDataWrapper"
import { CreateEmployeeButton } from "./CreateEmployeeButton"
import { useExampleData } from "./ExampleContext"

type EmployeeManagementProps = {
    className?: string,
}

export const EmployeeManagement = ({className}: EmployeeManagementProps) => {
    const [{
        selectedOrganization,
        selectedDepartment,
        employees,
    }, dispatch] = useExampleData();

    const queryParameters = selectedDepartment === undefined
        ? { organizationId: selectedOrganization!.id }
        : { departmentId: selectedDepartment.id }

    const setEmployees = useCallback(
        (employees: Employee[]) => dispatch({type: 'set-employees', employees}),
        [dispatch],
    );

    return <>
        <Row className={className}>
            <Col>
                <ApiResource resource='/employees' queryParameters={queryParameters} headers={{'api-version': '2.0'}} renderError={renderEmployeesError}>
                    <HoistDataWrapper setData={setEmployees}>
                        <EmployeeList employees={employees}/>
                    </HoistDataWrapper>
                </ApiResource>
            </Col>
        </Row>
        <Row>
            <Col>
                <CreateEmployeeButton className='mt-3' color='primary'>
                    Create Employee
                </CreateEmployeeButton>
            </Col>
        </Row>
    </>
}

const renderEmployeesError: ErrorRenderer = (error, {renderDefault}) => (
    error.status === 404
        ? <EmployeeList employees={[]}/>
        : renderDefault(error)
)