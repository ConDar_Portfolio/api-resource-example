import { WretchError } from "wretch";
import { LoadingSpinner } from "../../common/LoadingSpinner";
import { useApiResource } from "../useApiResource";
import { renderApiResourceChildren } from "./helpers";
import { ApiResourceChildren, ApiResourceRenderProps, ApiResourceRequestProps } from "./types";

type ApiResourceProps<T> = ApiResourceRequestProps & ApiResourceRenderProps & {
    children: ApiResourceChildren<T>,
}

export const ApiResource = <T extends unknown>(props: ApiResourceProps<T>) => {
    const {
        resource,
        queryParameters,
        headers,

        renderLoading = defaultRenderLoading,
        renderError = defaultRenderError,

        children,
    } = props;

    const [state, reloadResource] = useApiResource<T>(resource, {queryParameters, headers});

    switch (state.status) {
        case 'pending':
            return <>{renderLoading()}</>

        case 'error':
            console.log(state.error);
            return <>{renderError(state.error, {renderDefault: defaultRenderError, reload: reloadResource})}</>

        case 'success':
            return <>{renderApiResourceChildren(children, state.data, reloadResource)}</>
    }
}

const defaultRenderLoading = () => <LoadingSpinner/>

const defaultRenderError = (error: WretchError) => (
    <h1 className='display-3 text-danger'>
        Error with status code {error.status}
    </h1>
)