import { useCallback, useState } from "react";
import { Department, Organization, VersionExample } from "../common/types";
import { DepartmentManagement } from "./DepartmentManagement";
import { EmployeeManagement } from "./EmployeeManagement";
import { OrganizationManagement } from "./OrganizationManagement";

const Example = () => {
    const [selectedOrganization, setSelectedOrganization] = useState<Organization | undefined>(undefined);
    const [selectedDepartment, setSelectedDepartment] = useState<Department | undefined>(undefined);

    const setOrganization = useCallback((organization: Organization | undefined) => {
        setSelectedOrganization(organization);
        setSelectedDepartment(undefined);
    }, [setSelectedOrganization, setSelectedDepartment]);

    return <>
        <OrganizationManagement setOrganization={setOrganization}/>
        {selectedOrganization && <>
            <DepartmentManagement
                className='mt-4'
                organization={selectedOrganization}
                setDepartment={setSelectedDepartment}/>
            <EmployeeManagement
                className='mt-4'
                organization={selectedOrganization}
                department={selectedDepartment}/>
        </>}
    </>
}

export const versionSeven: VersionExample = {
    version: 7,
    title: 'Hoist the reload functionality',
    description: <>
        <p>
            This version considers a new requirement to create a new employee, and once it is added automatically refresh the already loaded list of employees.
            This change requires quite an involved change, but sets up some improved structures to make some changes in future versions easier.
        </p>
        <p>
            The first change is the addition of the <code>useApi</code> hook which returns the default configuration of a <code>Wretch</code> API object.
            Previously all setup of an api was handled in the <code>useApiResource</code> hook, but now we need to access that setup in multiple places;
            in particular we now need to setup the <code>Wretch</code> in both the <code>useApiResource</code> hook and the new
            {} <code>CreateEmployeeButton</code> component.
        </p>
        <p>
            The next major change is a change in the type of the <code>children</code> of <code>ApiResource</code> and how they are rendered.
            The <code>children</code> prop is now of type <code>ApiResourceChildren{'<'}T{'>'}</code> which is either a function (as it was previously),
            or a <code>ReactElement</code> with props containing both <code>data</code> and <code>reload</code>, to handle the rendering of the new type
            a helper method is added to render them.
        </p>
        <p>
            Finally the wrapper component <code>HoistReloadWrapper</code> is introduced that, when rendered, will call it's
            {} <code>setReload</code> callback with the reload data function. The general use case of this allows the reload data function
            to be assigned outside of <code>ApiResource</code> component it's rendered within, allowing parent or sibling components
            to be able to trigger the reload of the <code>ApiResource</code>.
        </p>
        <p>
            By using the above this allows the <code>EmployeeManagement</code> component to be updated to hoist the reload data function
            of the <code>ApiResource</code> that loads the members. The hoisted reload function is then passed down to the
            {} <code>CreateEmployeeButton</code> which will allow it to trigger a reload of the eployees list when a new employee has been added.
        </p>
    </>,
    component: Example,
}