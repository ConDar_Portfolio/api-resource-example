import classNames from "classnames";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { Button, ButtonProps, Form, FormFeedback, FormGroup, Input, InputGroup, InputGroupText, Label, Modal, ModalBody, ModalHeader } from "reactstrap";
import { Department, Organization } from "../common/types";
import { ApiResource } from "./ApiResource";
import { useApi } from "./useApi";

type CreateEmployeeButtonProps = ButtonProps & {
    reloadEmployees: () => void,
    organization: Organization,
    department?: Department
}

type NewEmployeeFormData = {
    firstName: string,
    lastName: string,
    departmentId: number,
    salary: number,
}

export const CreateEmployeeButton = ({reloadEmployees, organization, department, ...buttonProps}: CreateEmployeeButtonProps) => {
    const [isOpen, setIsOpen] = useState(false);
    const api = useApi();

    const {register, handleSubmit, reset, formState: {errors}, getValues} = useForm<NewEmployeeFormData>({
        defaultValues: {
            departmentId: department === undefined ? -1 : department.id,
        },
    });

    const onSubmit = handleSubmit(formData => {
        api.url('/employees')
            .post(formData)
            .res(reloadEmployees)
            .catch(console.error)
            .finally(() => {
                reset();
                setIsOpen(false);
            })
    });

    const toggleOpen = () => {
        reset({
            departmentId: department === undefined ? -1 : department.id,
        });
        setIsOpen(current => !current);
    }

    const {ref: firstNameRef, ...firstNameRest} = register('firstName', {required: true, maxLength: 50});
    const {ref: lastNameRef, ...lastNameRest} = register('lastName', {required: true, maxLength: 50});
    const {ref: departmentIdRef, ...departmentIdRest} = register('departmentId', {required: true, valueAsNumber: true, min: 1});
    const {ref: salaryRef, ...salaryRest} = register('salary', {required: true, valueAsNumber: true, min: 1, validate: salary => !isNaN(salary)});

    console.log(getValues())

    return <>
        <Button {...buttonProps} onClick={toggleOpen}/>
        <Modal isOpen={isOpen} toggle={toggleOpen}>
            <ModalHeader>Create new employee</ModalHeader>
            <ModalBody>
                <Form onSubmit={onSubmit}>
                    <FormGroup>
                        <Label for='firstName'>First name:</Label>
                        <Input id='firstName' type='text' invalid={errors.firstName !== undefined} innerRef={firstNameRef} {...firstNameRest}/>
                        <FormFeedback>
                            First name is required and must be at most 50 characters
                        </FormFeedback>
                    </FormGroup>
                    <FormGroup>
                        <Label for='lastName'>Last name:</Label>
                        <Input id='lastName' type='text' invalid={errors.lastName !== undefined} innerRef={lastNameRef} {...lastNameRest}/>
                        <FormFeedback>
                            Last name is required and must be at most 50 characters
                        </FormFeedback>
                    </FormGroup>
                    <FormGroup>
                        <ApiResource resource='/departments' queryParameters={{organizationId: organization.id}}>
                            {(departments: Department[]) => <>
                                <Label for='departmentId'>Department:</Label>
                                <Input id='departmentId' type='select' invalid={errors.departmentId !== undefined} innerRef={departmentIdRef} {...departmentIdRest}>
                                    <option value={-1} disabled>Select department....</option>
                                    {departments.map(department => (
                                        <option key={department.id} value={department.id}>{department.name}</option>
                                    ))}
                                </Input>
                                <FormFeedback>
                                    A department id must be provided.
                                </FormFeedback>
                            </>}
                        </ApiResource>
                    </FormGroup>
                    <FormGroup>
                        <Label for='salary'>Salary:</Label>
                        <InputGroup>
                            <InputGroupText>£</InputGroupText>
                            <Input id='salary' type='text' invalid={errors.salary !== undefined} innerRef={salaryRef} {...salaryRest}/>
                        </InputGroup>
                        <FormFeedback className={classNames({'d-block': errors.salary !== undefined})}>
                            A salary of at least £1 is required
                        </FormFeedback>
                    </FormGroup>
                    <FormGroup>
                        <Button color='primary' type='submit'>Create</Button>
                    </FormGroup>
                </Form>
            </ModalBody>
        </Modal>
    </>
}