import { useMemo } from "react"
import wretch, { Wretch } from "wretch"
import { config } from "../../config"
import { BasicAuthAddon } from "../common/BasicAuthAddon"

const createApi = (): Wretch => {
    // any cast required to stop typescript complaining
    return wretch(config.api.url)
        .addon(BasicAuthAddon)
        .basicAuth(config.api.auth.username, config.api.auth.password) as any;
}

export const useApi = (): Wretch => useMemo(createApi, []);