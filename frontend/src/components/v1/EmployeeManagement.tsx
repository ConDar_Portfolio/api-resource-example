import { useEffect, useState } from "react"
import { Col, Row } from "reactstrap"
import wretch, { WretchError } from "wretch"
import QueryStringAddon from "wretch/addons/queryString"
import { config } from "../../config"
import { BasicAuthAddon } from "../common/BasicAuthAddon"
import { EmployeeList } from "../common/EmployeeList"
import { LoadingSpinner } from "../common/LoadingSpinner"
import { Department, Employee, Organization } from "../common/types"

type EmployeeManagementProps = {
    organization: Organization,
    department: Department | undefined,
    className?: string,
}

type FetchState =
    | { status: 'pending' }
    | { status: 'success', data: Employee[] }
    | { status: 'error', error: WretchError }

export const EmployeeManagement = ({organization, department, className}: EmployeeManagementProps) => {
    const [state, setState] = useState<FetchState>({status: 'pending'});

    useEffect(() => {
        setState({status: 'pending'});

        const api = wretch(config.api.url)
            .addon(BasicAuthAddon)
            .basicAuth(config.api.auth.username, config.api.auth.password);

        api.url('/employees')
            .addon(QueryStringAddon)
            .query(department === undefined
                ? { organizationId: organization.id }
                : { departmentId: department.id })
            .get()
            .json((employees: Employee[]) => {
                setState({status: 'success', data: employees});
                return employees;
            })
            .catch((error: WretchError) => setState({status: 'error', error}));
    }, [setState, organization.id, department])

    if (state.status === 'pending')
        return <LoadingSpinner/>

    if (state.status === 'error') {
        console.error(state.error);
        return (
            <h1 className='display-e text-danger'>
                Error with status code {state.error.status}
            </h1>
        )
    }

    return (
        <Row className={className}>
            <Col>
                <EmployeeList employees={state.data}/>
            </Col>
        </Row>
    )
}