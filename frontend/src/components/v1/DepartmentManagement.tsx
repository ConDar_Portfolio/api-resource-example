import classNames from "classnames";
import { useEffect, useState } from "react";
import { Col, Row } from "reactstrap";
import wretch, { WretchError } from "wretch";
import QueryStringAddon from "wretch/addons/queryString";
import { config } from "../../config";
import { BasicAuthAddon } from "../common/BasicAuthAddon";
import { DepartmentSelect } from "../common/DepartmentSelect";
import { LoadingSpinner } from "../common/LoadingSpinner";
import { Department, Organization } from "../common/types";

type DepartmentManagementProps = {
    organization: Organization,
    setDepartment: (department: Department | undefined) => void,
    className?: string,
}

type FetchState =
    | { status: 'pending' }
    | { status: 'success', data: Department[] }
    | { status: 'error', error: WretchError }

export const DepartmentManagement = ({organization, setDepartment, className}: DepartmentManagementProps) => {
    const [state, setState] = useState<FetchState>({status: 'pending'});

    useEffect(() => {
        setState({status: 'pending'});

        const api = wretch(config.api.url)
            .addon(BasicAuthAddon)
            .basicAuth(config.api.auth.username, config.api.auth.password);

        api.url('/departments')
            .addon(QueryStringAddon)
            .query({
                organizationId: organization.id,
            })
            .get()
            .json((departments: Department[]) => {
                setState({status: 'success', data: departments});
                return departments;
            })
            .catch((error: WretchError) => setState({status: 'error', error}));
    }, [setState, organization.id]);

    if (state.status === 'pending')
        return <LoadingSpinner/>

    if (state.status === 'error') {
        console.error(state.error);
        return (
            <h1 className='display-3 text-danger'>
                Error with status code {state.error.status}
            </h1>
        )
    }

    return (
        <Row className={classNames(className, 'justify-content-end')}>
            <Col xs={4}>
                <DepartmentSelect departments={state.data} onSelected={setDepartment}/>
            </Col>
        </Row>
    )
}