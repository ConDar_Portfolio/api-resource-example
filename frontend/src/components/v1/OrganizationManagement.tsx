import { useCallback, useEffect, useState } from "react";
import { Col, Row } from "reactstrap";
import wretch, { WretchError } from "wretch";
import { config } from "../../config";
import { BasicAuthAddon } from "../common/BasicAuthAddon";
import { LoadingSpinner } from "../common/LoadingSpinner";
import { OrganizationCard } from "../common/OrganizationCard";
import { OrganizationSelect } from "../common/OrganizationSelect";
import { Organization } from "../common/types";

type OrganizationManagementProps = {
    setOrganization: (organization: Organization | undefined) => void,
    className?: string,
}

type FetchState =
    | { status: 'pending' }
    | { status: 'success', data: Organization[] }
    | { status: 'error', error: WretchError }

export const OrganizationManagement = ({setOrganization, className}: OrganizationManagementProps) => {
    const [state, setState] = useState<FetchState>({status: 'pending'});
    const [selectedOrganization, setSelectedOrganization] = useState<Organization | undefined>(undefined);

    const onSelected = useCallback((organization: Organization) => {
        setSelectedOrganization(organization);
        setOrganization(organization);
    }, [setSelectedOrganization, setOrganization]);

    useEffect(() => {
        setState({status: 'pending'});

        const api = wretch(config.api.url)
            .addon(BasicAuthAddon)
            .basicAuth(config.api.auth.username, config.api.auth.password);

        api.url('/organizations')
            .get()
            .json((organizations: Organization[]) => {
                setState({status: 'success', data: organizations});
                return organizations;
            })
            .catch((error: WretchError) => setState({status: 'error', error}));
    }, [setState]);

    if (state.status === 'pending')
        return <LoadingSpinner/>

    if (state.status === 'error') {
        console.error(state.error);
        return (
            <h1 className='display-3 text-danger'>
                Error with status code {state.error.status}
            </h1>
        )
    }

    return (
        <Row className={className}>
            <Col>
                <OrganizationSelect organizations={state.data} onSelected={onSelected}/>
            </Col>
            {selectedOrganization && (
                <Col>
                    <OrganizationCard organization={selectedOrganization}/>
                </Col>
            )}
        </Row>
    )
}