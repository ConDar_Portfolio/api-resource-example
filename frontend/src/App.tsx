import { Container } from "reactstrap";
import { ApiResourceExamples } from "./components/ApiResourceExamples";

export const App = () => (
    <Container>
        <ApiResourceExamples/>
    </Container>
)