﻿using System.Data;
using Dapper;
using FluentValidation;
using SampleWebApi.Models;
using SampleWebApi.Requests;

namespace SampleWebApi.Endpoints;

internal static class OrganizationEndpoints
{
    public static void Configure(IEndpointRouteBuilder builder)
    {
        builder.MapGet("/organizations", GetAllOrganizations)
            .RequireAuthorization();
        builder.MapGet("/organizations/{id:int:min(1)}", GetOrganization)
            .RequireAuthorization();
        builder.MapPost("/organizations", CreateOrganization)
            .RequireAuthorization();
        builder.MapDelete("/organization/{id:int:min(1)}", DeleteOrganization)
            .RequireAuthorization();
    }

    private static async Task<IEnumerable<Organization>> GetAllOrganizations(IDbConnection connection)
        => await connection.QueryAsync<Organization>("SELECT * FROM Organization");

    private static async Task<IResult> GetOrganization(int id, IDbConnection connection)
    {
        var organization = await connection.QuerySingleOrDefaultAsync<Organization>(
            "SELECT * FROM Organization WHERE Id = @id",
            new { id });

        return organization is null
            ? Results.NotFound()
            : Results.Ok(organization);
    }

    private static async Task<IResult> CreateOrganization(
        CreateOrganizationRequest request,
        IValidator<CreateOrganizationRequest> validator,
        IDbConnection connection)
    {
        var validationResult = await validator.ValidateAsync(request);

        if (!validationResult.IsValid)
            return Results.ValidationProblem(validationResult.ToDictionary());

        var createdOrganization = await connection.QuerySingleAsync<Organization>(
            @"INSERT INTO Organization(Name, Type, Revenue)
              VALUES(@Name, @Type, @Revenue)
              RETURNING *",
            request);

        return Results.Created($"/organizations/{createdOrganization.Id}", createdOrganization);
    }

    private static async Task<IResult> DeleteOrganization(int id, IDbConnection connection)
    {
        using var transaction = connection.BeginTransaction();

        var rowsAffected = await connection.ExecuteAsync(
            @"DELETE FROM Organization
              WHERE Id = @id
              ",
            new { id });

        if (rowsAffected != 1)
        {
            transaction.Rollback();
            return Results.Problem($"Error while deleting organization with id {id}");
        }

        transaction.Commit();
        return Results.NoContent();
    }
}
