﻿namespace SampleWebApi.Endpoints;

internal static class PingEndpoints
{
    public static void Configure(IEndpointRouteBuilder builder)
    {
        builder.MapGet("/ping", PingEndpoint);
        builder.MapGet("/ping/authed", AuthedPingEndpoint)
            .RequireAuthorization();
    }

    private static string PingEndpoint()
        => $"You have reached the ping endpoint at {DateTimeOffset.UtcNow:f}";

    private static string AuthedPingEndpoint()
        => $"You have reached the authed ping endpoint at {DateTimeOffset.UtcNow:f}";
}
