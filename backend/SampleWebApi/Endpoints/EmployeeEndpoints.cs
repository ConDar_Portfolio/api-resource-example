﻿using System.Data;
using Asp.Versioning;
using Asp.Versioning.Builder;
using Dapper;
using FluentValidation;
using SampleWebApi.Models;
using SampleWebApi.Requests;

namespace SampleWebApi.Endpoints;

internal static class EmployeeEndpoints
{
    public static void Configure(IEndpointRouteBuilder builder, ApiVersionSet apiVersionSet)
    {
        builder.MapGet("/employees", GetEmployees)
            .RequireAuthorization()
            .WithApiVersionSet(apiVersionSet)
            .HasApiVersion(new ApiVersion(1, 0));
        builder.MapGet("/employees", GetEmployeesV2)
            .RequireAuthorization()
            .WithApiVersionSet(apiVersionSet)
            .HasApiVersion(new ApiVersion(2, 0));
        builder.MapGet("/employees/{id:int:min(1)}", GetEmployee)
            .RequireAuthorization();
        builder.MapPost("/employees", CreateEmployee)
            .RequireAuthorization()
            .WithApiVersionSet(apiVersionSet);
        builder.MapDelete("/employees/{id:int:min(1)}", DeleteEmployee)
            .RequireAuthorization();
    }

    private static async Task<IResult> GetEmployees(
        int? organizationId,
        int? departmentId,
        IValidator<GetEmployeesRequest> validator,
        IDbConnection connection)
    {
        var request = new GetEmployeesRequest
        {
            OrganizationId = organizationId,
            DepartmentId = departmentId,
        };

        var validationResult = await validator.ValidateAsync(request);

        if (!validationResult.IsValid)
            return Results.ValidationProblem(validationResult.ToDictionary());

        var employees = await GetEmployeesAsync(request, connection);

        return Results.Ok(employees);
    }

    private static async Task<IResult> GetEmployeesV2(
        int? organizationId,
        int? departmentId,
        IValidator<GetEmployeesRequest> validator,
        IDbConnection connection)
    {
        var request = new GetEmployeesRequest
        {
            OrganizationId = organizationId,
            DepartmentId = departmentId,
        };

        var validationResult = await validator.ValidateAsync(request);

        if (!validationResult.IsValid)
            return Results.ValidationProblem(validationResult.ToDictionary());

        var employees = (await GetEmployeesAsync(request, connection)).ToList();

        return employees.Any()
            ? Results.Ok(employees)
            : Results.NotFound();
    }

    private static async Task<IResult> GetEmployee(int id, IDbConnection connection)
    {
        var employee = await connection.QuerySingleOrDefaultAsync<Employee>(
            "SELECT * FROM Employee WHERE Id = @id",
            new { id });

        return employee is null
            ? Results.NotFound()
            : Results.Ok(employee);
    }

    private static async Task<IResult> CreateEmployee(
        CreateEmployeeRequest request,
        IValidator<CreateEmployeeRequest> validator,
        IDbConnection connection)
    {
        var validationResult = await validator.ValidateAsync(request);

        if (!validationResult.IsValid)
            return Results.ValidationProblem(validationResult.ToDictionary());

        var department = await connection.QuerySingleOrDefaultAsync<Department>(
            "SELECT * FROM Department WHERE Id = @departmentId",
            new { departmentId = request.DepartmentId });

        if (department is null)
            return Results.UnprocessableEntity($"Department with id {request.DepartmentId} does not exist");

        var createdEmployee = await connection.QuerySingleAsync<Employee>(
            @"INSERT INTO Employee(DepartmentId, FirstName, LastName, Salary)
              VALUES (@DepartmentId, @FirstName, @LastName, @Salary)
              RETURNING *",
            request);

        return Results.Created($"/employees/{createdEmployee.Id}", createdEmployee);
    }

    private static async Task<IResult> DeleteEmployee(int id, IDbConnection connection)
    {
        using var transaction = connection.BeginTransaction();

        var rowsAffected = await connection.ExecuteAsync(
            @"DELETE FROM Employee
              WHERE Id = @id",
            new { id });

        if (rowsAffected != 1)
        {
            transaction.Rollback();
            return Results.Problem($"Error while deleting employee with id {id}");
        }

        transaction.Commit();
        return Results.NoContent();
    }

    private static async Task<IEnumerable<Employee>> GetEmployeesAsync(GetEmployeesRequest request, IDbConnection connection)
    {
        Func<Task<IEnumerable<Employee>>> getEmployeesImplementation = request.OrganizationId is not null
            ? () => GetEmployeesByOrganization(request.OrganizationId.Value, connection)
            : () => GetEmployeesByDepartment(request.DepartmentId!.Value, connection);

        return await getEmployeesImplementation.Invoke();
    }

    private static async Task<IEnumerable<Employee>> GetEmployeesByOrganization(int organizationId, IDbConnection connection)
        => await connection.QueryAsync<Employee>(
            @"SELECT Employee.*
              FROM Employee
              LEFT JOIN Department on Department.Id = Employee.DepartmentId
              WHERE Department.OrganizationId = @organizationId",
            new { organizationId });

    private static async Task<IEnumerable<Employee>> GetEmployeesByDepartment(int departmentId, IDbConnection connection)
        => await connection.QueryAsync<Employee>(
            "SELECT * FROM Employee WHERE DepartmentId = @departmentId",
            new { departmentId });
}
