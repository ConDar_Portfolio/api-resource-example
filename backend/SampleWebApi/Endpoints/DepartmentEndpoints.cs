﻿using System.ComponentModel.DataAnnotations;
using System.Data;
using Dapper;
using FluentValidation;
using Microsoft.AspNetCore.Components.RenderTree;
using Microsoft.AspNetCore.Mvc;
using SampleWebApi.Models;
using SampleWebApi.Requests;

namespace SampleWebApi.Endpoints;

internal static class DepartmentEndpoints
{
    public static void Configure(IEndpointRouteBuilder builder)
    {
        builder.MapGet("/departments", GetDepartments)
            .RequireAuthorization();
        builder.MapGet("/departments/{id:int:min(1)}", GetDepartment)
            .RequireAuthorization();
        builder.MapPost("/departments", CreateDepartment)
            .RequireAuthorization();
        builder.MapDelete("/departments/{id:int:min(1)}", DeleteDepartment)
            .RequireAuthorization();
    }

    private static async Task<IEnumerable<Department>> GetDepartments([Required] int organizationId, IDbConnection connection)
        => await connection.QueryAsync<Department>(
            "SELECT * FROM Department WHERE OrganizationId = @organizationId",
            new { organizationId });

    private static async Task<IResult> GetDepartment(int id, IDbConnection connection)
    {
        var department = await connection.QuerySingleOrDefaultAsync<Department>(
            "SELECT * FROM Department WHERE Id = @id",
            new { id });

        return department is null
            ? Results.NotFound()
            : Results.Ok(department);
    }

    private static async Task<IResult> CreateDepartment(
        CreateDepartmentRequest request,
        IValidator<CreateDepartmentRequest> validator,
        IDbConnection connection)
    {
        var validationResult = await validator.ValidateAsync(request);

        if (!validationResult.IsValid)
            return Results.ValidationProblem(validationResult.ToDictionary());

        var organization = await connection.QuerySingleOrDefaultAsync<Organization>(
            "SELECT * FROM Organization WHERE Id = @organizationId",
            new { organizationId = request.OrganizationId });

        if (organization is null)
            return Results.UnprocessableEntity($"Organization with id {request.OrganizationId} does not exist");

        var createdDepartment = await connection.QuerySingleAsync<Department>(
            @"INSERT INTO Department(OrganizationId, Name)
              VALUES(@OrganizationId, @Name)
              RETURNING *",
            request);

        return Results.Created($"/departments/{createdDepartment.Id}", createdDepartment);
    }

    private static async Task<IResult> DeleteDepartment(int id, IDbConnection connection)
    {
        using var transaction = connection.BeginTransaction();

        var rowsAffected = await connection.ExecuteAsync(
            @"DELETE FROM Department
              WHERE Id = @id",
            new { id });

        if (rowsAffected != 1)
        {
            transaction.Rollback();
            return Results.Problem($"Error while deleting department with id {id}");
        }

        transaction.Commit();
        return Results.NoContent();
    }
}
