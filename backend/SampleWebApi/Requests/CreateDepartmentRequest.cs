﻿using System.Data;
using Dapper;
using FluentValidation;
using SampleWebApi.Models;

namespace SampleWebApi.Requests;

public sealed record CreateDepartmentRequest
{
    public int OrganizationId { get; init; }
    public string Name { get; init; } = default!;
}

public sealed class CreateDepartmentRequestValidator : AbstractValidator<CreateDepartmentRequest>
{
    public CreateDepartmentRequestValidator()
    {
        RuleFor(request => request.OrganizationId)
            .GreaterThan(0);

        RuleFor(request => request.Name)
            .Cascade(CascadeMode.Stop)
            .NotEmpty()
            .MaximumLength(25);
    }
}
