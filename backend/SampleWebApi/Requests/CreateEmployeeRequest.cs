﻿using FluentValidation;

namespace SampleWebApi.Requests;

public sealed record CreateEmployeeRequest
{
    public int DepartmentId { get; init; }
    public string FirstName { get; init; } = default!;
    public string LastName { get; set; } = default!;
    public decimal Salary { get; init; }
}

public sealed class CreateEmployeeRequestValidator : AbstractValidator<CreateEmployeeRequest>
{
    public CreateEmployeeRequestValidator()
    {
        RuleFor(request => request.DepartmentId)
            .GreaterThan(0);

        RuleFor(request => request.FirstName)
            .Cascade(CascadeMode.Stop)
            .NotEmpty()
            .MaximumLength(50);

        RuleFor(request => request.LastName)
            .Cascade(CascadeMode.Stop)
            .NotEmpty()
            .MaximumLength(50);

        RuleFor(request => request.Salary)
            .GreaterThan(0);
    }
}
