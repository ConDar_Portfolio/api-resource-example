﻿using FluentValidation;
using Microsoft.AspNetCore.Mvc;

namespace SampleWebApi.Requests;

public sealed record GetEmployeesRequest
{
    [FromQuery]
    public int? OrganizationId { get; init; }

    [FromQuery]
    public int? DepartmentId { get; init; }
}

public sealed class GetEmployeesRequestValidator : AbstractValidator<GetEmployeesRequest>
{
    public GetEmployeesRequestValidator()
    {
        RuleFor(request => request)
            .Cascade(CascadeMode.Stop)
            .Must(NotHaveBothOrganizationAndDepartmentId)
            .WithMessage("You must provide only one of organizationId or departmentId")
            .Must(HaveAtLeastOneOfOrganizationAndDepartmentId)
            .WithMessage("You must provide one of organizationId or departmentId");
    }

    private static bool NotHaveBothOrganizationAndDepartmentId(GetEmployeesRequest request)
        => request.DepartmentId is null || request.OrganizationId is null;

    private static bool HaveAtLeastOneOfOrganizationAndDepartmentId(GetEmployeesRequest request)
        => request.DepartmentId is not null || request.OrganizationId is not null;
}
