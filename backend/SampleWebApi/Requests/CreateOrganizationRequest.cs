﻿using FluentValidation;

namespace SampleWebApi.Requests;

public sealed record CreateOrganizationRequest
{
    public string Name { get; init; } = default!;
    public string Type { get; init; } = default!;
    public decimal Revenue { get; init; }
}

public sealed class CreateOrganizationRequestValidator : AbstractValidator<CreateOrganizationRequest>
{
    public CreateOrganizationRequestValidator()
    {
        RuleFor(request => request.Name)
            .Cascade(CascadeMode.Stop)
            .NotEmpty()
            .MaximumLength(50);

        RuleFor(request => request.Type)
            .Cascade(CascadeMode.Stop)
            .NotEmpty()
            .MaximumLength(25);

        RuleFor(request => request.Revenue)
            .GreaterThanOrEqualTo(0);
    }
}
