﻿using System.Net;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;

namespace SampleWebApi;

internal sealed class BasicAuthScheme : AuthenticationHandler<AuthenticationSchemeOptions>
{
    private const string AuthorizationHeaderName = "Authorization";
    private const string DefaultAuthenticationFailMessage = $"Invalid {AuthorizationHeaderName} header";
    private const string ExpectedHeaderPrefix = "Basic ";

    private readonly BasicAuthOptions _basicAuthOptions;

    public BasicAuthScheme(
        IOptionsMonitor<AuthenticationSchemeOptions> options,
        ILoggerFactory logger,
        UrlEncoder encoder,
        ISystemClock clock,
        IOptions<BasicAuthOptions> basicAuthOptions
    ) : base(options, logger, encoder, clock)
    {
        _basicAuthOptions = basicAuthOptions.Value;
    }

    protected override Task<AuthenticateResult> HandleAuthenticateAsync()
    {
        try
        {
            return Task.FromResult(HandleAuthenticate());
        }
        catch (AuthenticationException exception)
        {
            var failMessage = string.IsNullOrWhiteSpace(exception.Message)
                ? DefaultAuthenticationFailMessage
                : exception.Message;

            return Task.FromResult(AuthenticateResult.Fail(failMessage));
        }
    }

    private AuthenticateResult HandleAuthenticate()
    {
        var authHeader = Request.Headers[AuthorizationHeaderName].ToString();

        if (!IsValidAuthHeader(authHeader))
        {
            Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            return AuthenticateResult.Fail(DefaultAuthenticationFailMessage);
        }

        var credentials = ExtractCredentials(authHeader);

        if (!AreCredentialsValid(credentials))
        {
            Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            return AuthenticateResult.Fail(DefaultAuthenticationFailMessage);
        }

        var authenticationTicket = CreateAuthenticationTicket(credentials);

        return AuthenticateResult.Success(authenticationTicket);
    }

    private static bool IsValidAuthHeader(string? authHeader)
        => authHeader is not null && authHeader.StartsWith(ExpectedHeaderPrefix, StringComparison.Ordinal);

    private static Credentials ExtractCredentials(string authHeader)
    {
        var token = authHeader[ExpectedHeaderPrefix.Length..].Trim();
        var tokenBytes = Convert.FromBase64String(token);
        var credentialsString = Encoding.UTF8.GetString(tokenBytes);
        var credentialParts = credentialsString.Split(':');

        if (credentialParts.Length != 2)
            throw new AuthenticationException();

        return new Credentials(credentialParts[0], credentialParts[1]);
    }

    private bool AreCredentialsValid(Credentials credentials)
        => credentials.Username == _basicAuthOptions.ExpectedUsername
            && credentials.Password == _basicAuthOptions.ExpectedPassword;

    private AuthenticationTicket CreateAuthenticationTicket(Credentials credentials)
    {
        var claims = new[]
        {
            new Claim("user", credentials.Username),
        };

        var identity = new ClaimsIdentity(claims, "Basic");
        var claimsPrincipal = new ClaimsPrincipal(identity);

        return new AuthenticationTicket(claimsPrincipal, Scheme.Name);
    }

    private record Credentials(string Username, string Password);

    private class AuthenticationException : Exception { }
}

internal record BasicAuthOptions
{
    public string ExpectedUsername { get; set; } = default!;
    public string ExpectedPassword { get; set; } = default!;
}
