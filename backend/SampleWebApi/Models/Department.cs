﻿namespace SampleWebApi.Models;

public sealed record Department
{
    public int Id { get; init; }
    public int OrganizationId { get; init; }
    public string Name { get; init; } = default!;
}
