﻿namespace SampleWebApi.Models;

public sealed record Employee
{
    public int Id { get; init; }
    public int DepartmentId { get; init; }
    public string FirstName { get; init; } = default!;
    public string LastName { get; init; } = default!;
    public decimal Salary { get; init; }
}
