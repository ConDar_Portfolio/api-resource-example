﻿namespace SampleWebApi.Models;

public sealed record Organization
{
    public int Id { get; init; }
    public string Name { get; init; } = default!;
    public string Type { get; init; } = default!;
    public decimal Revenue { get; init; }
}
