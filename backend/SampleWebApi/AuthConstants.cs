﻿namespace SampleWebApi;

internal static class AuthConstants
{
    public const string BasicAuthScheme = "basic-auth-scheme";
    public const string BasicAuthPolicy = "basic-auth-policy";
}
