﻿using Asp.Versioning;
using SampleWebApi.Endpoints;

namespace SampleWebApi.Startup;

internal static class EndpointConfiguration
{
    public static void ConfigureEndpoints(this IEndpointRouteBuilder builder)
    {
        var apiVersionSet = builder.NewApiVersionSet()
            .HasApiVersion(new ApiVersion(1, 0))
            .HasApiVersion(new ApiVersion(2, 0))
            .ReportApiVersions()
            .Build();

        PingEndpoints.Configure(builder);
        OrganizationEndpoints.Configure(builder);
        DepartmentEndpoints.Configure(builder);
        EmployeeEndpoints.Configure(builder, apiVersionSet);
    }
}
