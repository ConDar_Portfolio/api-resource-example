﻿using System.Data;
using System.Reflection;
using Asp.Versioning;
using FluentValidation;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Data.Sqlite;

namespace SampleWebApi.Startup;

internal static class ServicesConfiguration
{

    public static void ConfigureServices(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .ConfigureApiVersioning()
            .ConfigureSwagger()
            .ConfigureAuthentication()
            .ConfigureAuthorization(configuration.GetSection("auth"))
            .ConfigureDataLayer(configuration)
            .ConfigureValidation()
            .ConfigureCors();
    }

    private static IServiceCollection ConfigureApiVersioning(this IServiceCollection services)
        => services
            .AddApiVersioning(
                options =>
                {
                    options.DefaultApiVersion = new ApiVersion(1, 0);
                    options.ReportApiVersions = true;
                    options.AssumeDefaultVersionWhenUnspecified = true;
                    options.ApiVersionReader = new HeaderApiVersionReader("api-version");
                })
            .Services;

    private static IServiceCollection ConfigureSwagger(this IServiceCollection services)
        => services
            .AddEndpointsApiExplorer()
            .AddSwaggerGen(
                options =>
                {
                    options.ResolveConflictingActions(apis => apis.First());
                });

    private static IServiceCollection ConfigureAuthentication(this IServiceCollection services)
        => services
            .AddAuthentication()
            .AddScheme<AuthenticationSchemeOptions, BasicAuthScheme>(
                AuthConstants.BasicAuthScheme,
                "Basic Authentication",
                null)
            .Services;

    private static IServiceCollection ConfigureAuthorization(this IServiceCollection services, IConfiguration configuration)
        => services
            .AddAuthorization(ConfigureAuthorizationOptions)
            .Configure<BasicAuthOptions>(configuration.GetSection("basic"));

    private static IServiceCollection ConfigureDataLayer(this IServiceCollection services, IConfiguration configuration)
    {
        var connectionString = configuration.GetConnectionString("sqlite");

        return services
            .AddTransient<IDbConnection>(_ => new SqliteConnection(connectionString));
    }

    private static IServiceCollection ConfigureValidation(this IServiceCollection services)
        => services
            .AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());

    private static IServiceCollection ConfigureCors(this IServiceCollection service)
        => service
            .AddCors(
                options =>
                {
                    options.AddDefaultPolicy(
                        policy =>
                        {
                            policy.AllowAnyHeader()
                                .AllowAnyMethod()
                                .AllowAnyOrigin();
                        });
                });

    private static void ConfigureAuthorizationOptions(AuthorizationOptions options)
    {
        options.AddPolicy(
            AuthConstants.BasicAuthPolicy,
            policy =>
            {
                policy.RequireAuthenticatedUser();
                policy.AddAuthenticationSchemes(AuthConstants.BasicAuthScheme);
            });

        options.DefaultPolicy = options.GetPolicy(AuthConstants.BasicAuthPolicy)!;
    }
}
