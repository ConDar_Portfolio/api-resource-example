﻿namespace SampleWebApi.Startup;

internal static class MiddlewareConfiguration
{
    public static void ConfigureMiddleware(this IApplicationBuilder builder)
    {
        builder
            .UseSwagger()
            .UseSwaggerUI()
            .UseCors()
            .UseAuthentication()
            .UseAuthorization();
    }
}
