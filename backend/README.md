# Getting Started
To get started you should just be able to run the API with the provided `launchsettings.json` file.
You can re-initialize the database by running the `create-database.sql` file against the `data.sqlite` file (SQLite database), this does a full tear down and recreation of the database.

# Project Structure
The project structure here is fairly standard, however instead of controllers I went the route of using the minimal API system to define the endpoints that are accessible.
The API contains the `BasicAuthScheme` class which is an implementation of `AuthenticationHandler` to add basic http auth to the requests as there doesn't appear to be a standard built in ASP.Net Core implementation.

# Postman
A postman collection is included at the root of the `backend` directory, it does not cover every endpoint available in the API, but does cover the ones that are used by the `frontend` code.