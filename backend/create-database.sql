﻿DROP TABLE IF EXISTS Employee;
DROP TABLE IF EXISTS Department;
DROP TABLE IF EXISTS Organization;

CREATE TABLE Organization(
    Id INTEGER PRIMARY KEY AUTOINCREMENT,
    Name TEXT,
    Type TEXT,
    Revenue REAL
);

CREATE TABLE Department(
    Id INTEGER PRIMARY KEY AUTOINCREMENT,
    OrganizationId INTEGER,
    Name TEXT,
    FOREIGN KEY(OrganizationId) REFERENCES Organization(Id)
);

CREATE TABLE Employee(
    Id INTEGER PRIMARY KEY AUTOINCREMENT,
    DepartmentId INTEGER,
    FirstName TEXT,
    LastName TEXT,
    Salary REAL,
    FOREIGN KEY(DepartmentId) REFERENCES Department(Id)
);

INSERT INTO Organization (Name, Type, Revenue) VALUES
    ('Marvelous Communication Co.', 'Telecoms', 457930),
    ('D & C Mattresses Inc.', 'Bedding', 648924.33);

INSERT INTO Department (OrganizationId, Name) VALUES
    (1, 'Sales'),
    (1, 'Tech Support'),
    (2, 'Marketing'),
    (2, 'Sales'),
    (2, 'Manufacturing');

INSERT INTO Employee (DepartmentId, FirstName, LastName, Salary) VALUES
    (1, 'Steve', 'Rogers', 34000),
    (1, 'Bucky', 'Barnes', 37000),
    (2, 'Tony', 'Stark', 42000),
    (4, 'Barbara', 'Gordon', 28000),
    (4, 'Richard', 'Grayson', 28000),
    (4, 'Conner', 'Kent', 28000),
    (5, 'Clark', 'Kent', 34000),
    (6, 'Bruce', 'Wane', 34000);